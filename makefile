VENV ?= venv
PYTHON ?= $(VENV)/bin/python3
UVICORN ?= $(VENV)/bin/uvicorn
DOCKER ?= docker
IMAGE ?= dyff-api
GID ?= $(shell id -g)
UID ?= $(shell id -u)


BASE_DIR = $(shell pwd)
PYTHONPATH = $(BASE_DIR)
OPENAPI_DIR = $(BASE_DIR)/openapi
OPENAPI_JSON = $(OPENAPI_DIR)/dyff.json
OPENAPI_CLIENT_BASE_DIR ?= $(BASE_DIR)/client/python
OPENAPI_CLIENT_PACKAGE_DIR = $(OPENAPI_CLIENT_BASE_DIR)/dyff/client
OPENAPI_NAMESPACE = _generated
OPENAPI_GENERATED_DIR = $(OPENAPI_CLIENT_PACKAGE_DIR)/$(OPENAPI_NAMESPACE)

OPENAPI_CLIENT_SCALA_BASE_DIR ?= $(BASE_DIR)/client/scala
OPENAPI_CLIENT_SCALA_SCHEMA_DIR = $(OPENAPI_CLIENT_SCALA_BASE_DIR)/src/main/scala/io/dyff/schema/models

.PHONY: all
all: serve

.PHONY: setup
setup: $(VENV)/requirements.txt

.PHONY: version
version:
	VIRTUAL_ENV=$(VENV) uv pip install -e $(BASE_DIR)

.PHONY: compile
compile:
	uv pip compile --python-version 3.9 --upgrade -o requirements.txt pyproject.toml

requirements.txt: pyproject.toml | $(VENV)
	uv pip compile --python-version 3.9 --upgrade -o requirements.txt pyproject.toml

$(VENV)/requirements.txt: requirements.txt | $(VENV)
	VIRTUAL_ENV=$(VENV) uv pip install -r requirements.txt
	cp -f requirements.txt $(VENV)/requirements.txt

$(VENV):
	uv venv $(VENV)

.PHONY: json-schema
json-schema:
	$(PYTHON) ./scripts/generate-json-schema.py schema/json-schema/dyff.json

.PHONY: openapi
openapi: version $(OPENAPI_JSON)

$(OPENAPI_DIR):
	mkdir -p $(OPENAPI_DIR)

$(OPENAPI_JSON): dyff/api/api.py | $(OPENAPI_DIR)
	PYTHONPATH=$(shell pwd) $(PYTHON) ./scripts/generate-openapi-definitions.py $(OPENAPI_JSON)
	$(PYTHON) ./scripts/fix-openapi.py $(OPENAPI_JSON)

.PHONY: openapi-local-build
openapi-local-build: $(OPENAPI_GENERATED_DIR)

$(OPENAPI_GENERATED_DIR): $(OPENAPI_JSON)
	npx autorest --v3 --input-file=$(OPENAPI_JSON) --python --python3-only --output-folder=$(OPENAPI_CLIENT_PACKAGE_DIR) --namespace=$(OPENAPI_NAMESPACE)
	isort $(OPENAPI_GENERATED_DIR)
	black $(OPENAPI_GENERATED_DIR)

.PHONY: scala-codegen
scala-codegen:
	rm -rf $(OPENAPI_CLIENT_SCALA_BASE_DIR)
	mkdir $(OPENAPI_CLIENT_SCALA_BASE_DIR)
	docker run --rm -u $(UID):$(GID) -v $(BASE_DIR):/local swaggerapi/swagger-codegen-cli-v3 generate \
    	-i /local/openapi/dyff.json \
    	-l scala \
    	-o /local/client/scala \
		-c /local/client/swagger-config-scala.json
	python scripts/scala-add-type-conversions.py --schema_dir="$(OPENAPI_CLIENT_SCALA_SCHEMA_DIR)"

.PHONY: clean
clean:
	rm -f $(OPENAPI_JSON)
	rm -rf $(OPENAPI_DIR)

.PHONY: distclean
distclean:
	rm -rf node_modules/ $(VENV)
	find -name __pycache__ -type d -exec rm -rf '{}' \;
	find -name \*.pyc -type f -exec rm -f '{}' \;

.PHONY: serve
serve:
	$(UVICORN) dyff.api.server:app

.PHONY: docker-build
docker-build:
	$(DOCKER) build -t $(IMAGE) .

.PHONY: docker-run
docker-run:
	$(DOCKER) run --rm -it $(IMAGE)

.PHONY: docker-run-bash
docker-run-bash:
	$(DOCKER) run --rm -it $(IMAGE) bash

.PHONY: config-listing
config-listing:
	PYTHONPATH=$(shell pwd) $(PYTHON) ./scripts/generate-config-listing.py > docs/configuration.rst
