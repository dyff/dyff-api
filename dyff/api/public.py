# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import functools
import json
from typing import TypeVar

import fastapi

from dyff import storage
from dyff.api import dynamic_import
from dyff.api.config import config
from dyff.schema.platform import (
    ArtifactURL,
    Dataset,
    Documentation,
    DyffEntity,
    Entities,
    EntityStatus,
    InferenceService,
    Measurement,
    Method,
    Model,
    Module,
    Report,
    Resources,
    SafetyCase,
)
from dyff.schema.requests import (
    DatasetQueryRequest,
    DyffEntityQueryRequest,
    InferenceServiceQueryRequest,
    MeasurementQueryRequest,
    MethodQueryRequest,
    ModelQueryRequest,
    ModuleQueryRequest,
    ReportQueryRequest,
    SafetyCaseQueryRequest,
)
from dyff.storage.backend.base.query import QueryBackend, Whitelist

from ._version import __version__


@functools.lru_cache()
def get_query_backend() -> QueryBackend:
    return dynamic_import.instantiate(config.api.query.backend)


def is_public_access(entity: DyffEntity) -> bool:
    access = entity.labels.get("dyff.io/access")
    return access == "public"


def check_public_access(entity: DyffEntity) -> None:
    if not is_public_access(entity):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_403_FORBIDDEN, f"entity {entity.id} is not public"
        )


QueryT = TypeVar("QueryT", bound=DyffEntityQueryRequest)


def query_for_public_access(query: QueryT) -> tuple[Whitelist, QueryT]:
    """Return a Whitelist and a modified query request that together will query
    all entities that match the original query request and are also marked for
    public access.

    The original query request is not modified.
    """
    # Anything with the public-access label is accessible, regardless of
    # which account owns it.
    whitelist = Whitelist.everything()
    # .labels is an Optional[str] containing a JSON document
    if query.labels is not None:
        labels_dict = json.loads(query.labels)
    else:
        labels_dict = {}
    labels_dict["dyff.io/access"] = "public"
    query = query.copy(update={"labels": json.dumps(labels_dict)})
    return whitelist, query


def _generate_unique_id(route: fastapi.routing.APIRoute) -> str:
    """Strip everything but the function name from the ``operationId``
    fields."""
    return route.name


app = fastapi.FastAPI(
    title="Dyff v0 public (unauthenticated) API",
    generate_unique_id_function=_generate_unique_id,
    redoc_url=None,
    version=__version__,
)


# ----------------------------------------------------------------------------


@app.get(
    f"/{Resources.Dataset}",
    tags=[Resources.Dataset],
    summary="Get all Datasets matching a query.",
    response_description="The Datasets matching the query.",
)
def datasets_query(
    query: DatasetQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> list[Dataset]:
    """Get all Datasets matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist, query = query_for_public_access(query)
    results = query_backend.query_datasets(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Dataset}/{{dataset_id}}",
    tags=[Resources.Dataset],
    summary="Get a Dataset by its key.",
    response_description="The Dataset with the given key.",
)
def datasets_get(
    dataset_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> Dataset:
    """Get a Dataset by its key.

    Raises a 404 error if no entity exists with that key.
    """
    dataset = query_backend.get_dataset(dataset_id)
    if dataset is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(dataset)
    return dataset


@app.get(
    f"/{Resources.Dataset}/{{dataset_id}}/documentation",
    tags=[Resources.Dataset],
    summary="Get the documentation associated with a Dataset",
    response_description="The documentation for the Dataset",
)
def datasets_documentation(
    dataset_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> Documentation:
    """Get the documentation associated with a Dataset.

    Raises a 404 error if no entity exists with that key.
    """
    dataset = query_backend.get_dataset(dataset_id)
    if dataset is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(dataset)

    documentation = query_backend.get_documentation(dataset_id)
    return documentation or Documentation()


# ----------------------------------------------------------------------------


@app.get(
    f"/{Resources.InferenceService}",
    tags=[Resources.InferenceService],
    summary="Get all InferenceServices matching a query.",
    response_description="The InferenceServices matching the query.",
)
def inferenceservices_query(
    query: InferenceServiceQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> list[InferenceService]:
    """Get all InferenceServices matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist, query = query_for_public_access(query)
    results = query_backend.query_inference_services(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.InferenceService}/{{service_id}}",
    tags=[Resources.InferenceService],
    summary="Get an InferenceService by its key.",
    response_description="The InferenceService with the given key.",
)
def inferenceservices_get(
    service_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> InferenceService:
    """Get an InferenceService by its key.

    Raises a 404 error if no entity exists with that key.
    """
    inferenceservice = query_backend.get_inference_service(service_id)
    if inferenceservice is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(inferenceservice)
    return inferenceservice


@app.get(
    f"/{Resources.InferenceService}/{{service_id}}/documentation",
    tags=[Resources.InferenceService],
    summary="Get the documentation associated with an InferenceService",
    response_description="The documentation for the InferenceService",
)
def inferenceservices_documentation(
    service_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> Documentation:
    """Get the documentation associated with an InferenceService.

    Raises a 404 error if no entity exists with that key.
    """
    service = query_backend.get_inference_service(service_id)
    if service is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(service)

    documentation = query_backend.get_documentation(service_id)
    return documentation or Documentation()


# ----------------------------------------------------------------------------


@app.get(
    f"/{Resources.Measurement}",
    tags=[Resources.Measurement],
    summary="Get all Measurements matching a query.",
    response_description="The Measurements matching the query.",
)
def measurements_query(
    query: MeasurementQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> list[Measurement]:
    """Get all Measurements matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist, query = query_for_public_access(query)
    results = query_backend.query_measurements(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Measurement}/{{measurement_id}}",
    tags=[Resources.Measurement],
    summary="Get a Measurement by its key.",
    response_description="The Measurement with the given key.",
)
def measurements_get(
    measurement_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> Measurement:
    """Get a Measurement by its key.

    Raises a 404 error if no entity exists with that key.
    """
    measurement = query_backend.get_measurement(measurement_id)
    if measurement is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(measurement)
    return measurement


@app.get(
    f"/{Resources.Measurement}/{{measurement_id}}/downlinks",
    tags=[Resources.Measurement],
    summary="Get a list of signed GET URLs from which measurement artifacts can be downloaded.",
    response_description="A list of signed GET URLs.",
)
def measurements_downlinks(
    measurement_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> list[ArtifactURL]:
    measurement = query_backend.get_measurement(measurement_id)
    if measurement is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no measurement {measurement_id}"
        )

    check_public_access(measurement)

    if measurement.status != EntityStatus.complete:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected measurement.status == {EntityStatus.complete}; got {measurement.status}",
        )

    return list(storage.artifact_downlinks(Entities.Measurement, measurement_id))


# ----------------------------------------------------------------------------


@app.get(
    f"/{Resources.Method}",
    tags=[Resources.Method],
    summary="Get all Methods matching a query.",
    response_description="The Methods matching the query.",
)
def methods_query(
    query: MethodQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> list[Method]:
    """Get all Methods matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist, query = query_for_public_access(query)
    results = query_backend.query_methods(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Method}/{{method_id}}",
    tags=[Resources.Method],
    summary="Get a Method by its key.",
    response_description="The Method with the given key.",
)
def methods_get(
    method_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> Method:
    """Get a Method by its key.

    Raises a 404 error if no entity exists with that key.
    """
    method = query_backend.get_method(method_id)
    if method is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(method)
    return method


@app.get(
    f"/{Resources.Method}/{{method_id}}/documentation",
    tags=[Resources.Method],
    summary="Get the documentation associated with a Method",
    response_description="The documentation for the Method",
)
def methods_documentation(
    method_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> Documentation:
    """Get the documentation associated with a Method.

    Raises a 404 error if no entity exists with that key.
    """
    method = query_backend.get_method(method_id)
    if method is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(method)

    documentation = query_backend.get_documentation(method_id)
    return documentation or Documentation()


# ----------------------------------------------------------------------------


@app.get(
    f"/{Resources.Model}",
    tags=[Resources.Model],
    summary="Get all Models matching a query.",
    response_description="The Models matching the query.",
)
def models_query(
    query: ModelQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> list[Model]:
    """Get all Models matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist, query = query_for_public_access(query)
    results = query_backend.query_models(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Model}/{{model_id}}",
    tags=[Resources.Model],
    summary="Get a Model by its key.",
    response_description="The Model with the given key.",
)
def models_get(
    model_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> Model:
    """Get a Model by its key.

    Raises a 404 error if no entity exists with that key.
    """
    model = query_backend.get_model(model_id)
    if model is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(model)
    return model


@app.get(
    f"/{Resources.Model}/{{model_id}}/documentation",
    tags=[Resources.Model],
    summary="Get the documentation associated with a Model",
    response_description="The documentation for the Model",
)
def models_documentation(
    model_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> Documentation:
    """Get the documentation associated with a Model.

    Raises a 404 error if no entity exists with that key.
    """
    model = query_backend.get_model(model_id)
    if model is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(model)

    documentation = query_backend.get_documentation(model_id)
    return documentation or Documentation()


# ----------------------------------------------------------------------------


@app.get(
    f"/{Resources.Module}",
    tags=[Resources.Module],
    summary="Get all Modules matching a query.",
    response_description="The Modules matching the query.",
)
def modules_query(
    query: ModuleQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> list[Module]:
    """Get all Modules matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist, query = query_for_public_access(query)
    results = query_backend.query_modules(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Module}/{{module_id}}",
    tags=[Resources.Module],
    summary="Get a Module by its key.",
    response_description="The Module with the given key.",
)
def modules_get(
    module_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> Module:
    """Get a Module by its key.

    Raises a 404 error if no entity exists with that key.
    """
    module = query_backend.get_module(module_id)
    if module is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(module)
    return module


@app.get(
    f"/{Resources.Module}/{{module_id}}/documentation",
    tags=[Resources.Module],
    summary="Get the documentation associated with a Module",
    response_description="The documentation for the Module",
)
def modules_documentation(
    module_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> Documentation:
    """Get the documentation associated with a Module.

    Raises a 404 error if no entity exists with that key.
    """
    module = query_backend.get_module(module_id)
    if module is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(module)

    documentation = query_backend.get_documentation(module_id)
    return documentation or Documentation()


# ----------------------------------------------------------------------------


@app.get(
    f"/{Resources.Report}",
    tags=[Resources.Report],
    summary="Get all Reports matching a query.",
    response_description="The Reports matching the query.",
)
def reports_query(
    query: ReportQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> list[Report]:
    """Get all Reports matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist, query = query_for_public_access(query)
    results = query_backend.query_reports(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Report}/{{report_id}}",
    tags=[Resources.Report],
    summary="Get a Report by its key.",
    response_description="The Report with the given key.",
)
def reports_get(
    report_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> Report:
    """Get a Report by its key.

    Raises a 404 error if no entity exists with that key.
    """
    report = query_backend.get_report(report_id)
    if report is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(report)
    return report


@app.get(
    f"/{Resources.Report}/{{report_id}}/downlinks",
    tags=[Resources.Report],
    summary="Get a list of signed GET URLs from which Report artifacts can be downloaded.",
    response_description="A list of signed GET URLs.",
)
def reports_downlinks(
    report_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> list[ArtifactURL]:
    report = query_backend.get_report(report_id)
    if report is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no report {report_id}"
        )

    check_public_access(report)

    if report.status != EntityStatus.complete:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected report.status == {EntityStatus.complete}; got {report.status}",
        )

    return list(storage.artifact_downlinks(Entities.Report, report_id))


# ----------------------------------------------------------------------------


@app.get(
    f"/{Resources.SafetyCase}",
    tags=[Resources.SafetyCase],
    summary="Get all SafetyCase entities matching a query.",
    response_description="The SafetyCase entities matching the query.",
)
def safetycases_query(
    query: SafetyCaseQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> list[SafetyCase]:
    """Get all SafetyCase entities matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist, query = query_for_public_access(query)
    results = query_backend.query_safetycases(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.SafetyCase}/{{safetycase_id}}",
    tags=[Resources.SafetyCase],
    summary="Get a SafetyCase by its key.",
    response_description="The SafetyCase with the given key.",
)
def safetycases_get(
    safetycase_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> SafetyCase:
    """Get a SafetyCase by its key.

    Raises a 404 error if no entity exists with that key.
    """
    safetycase = query_backend.get_safetycase(safetycase_id)
    if safetycase is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_public_access(safetycase)
    return safetycase


@app.get(
    f"/{Resources.SafetyCase}/{{safetycase_id}}/downlinks",
    tags=[Resources.SafetyCase],
    summary="Get a list of signed GET URLs from which safety case artifacts can be downloaded.",
    response_description="A list of signed GET URLs.",
)
def safetycases_downlinks(
    safetycase_id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
) -> list[ArtifactURL]:
    safetycase = query_backend.get_safetycase(safetycase_id)
    if safetycase is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no safetycase {safetycase_id}"
        )

    check_public_access(safetycase)

    if safetycase.status != EntityStatus.complete:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected safetycase.status == {EntityStatus.complete}; got {safetycase.status}",
        )

    return list(storage.artifact_downlinks(Entities.SafetyCase, safetycase_id))
