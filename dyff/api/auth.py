# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# mypy: disable-error-code="import-untyped"
import asyncio
import functools
from datetime import timedelta
from urllib.parse import urlencode

import fastapi
import starlette.status as status
from authlib.integrations.base_client.errors import MismatchingStateError
from authlib.integrations.starlette_client import OAuth
from fastapi import Depends, Request
from fastapi.responses import RedirectResponse
from starlette.middleware.sessions import SessionMiddleware

from dyff.api import dynamic_import, timestamp, tokens
from dyff.api.config import config
from dyff.api.mgmt import roles
from dyff.schema.platform import AccessGrant, APIFunctions, Entities, Resources
from dyff.storage.backend.base.auth import Account, AuthBackend, Identity

CONF_URL = "https://accounts.google.com/.well-known/openid-configuration"


@functools.lru_cache()
def get_auth_backend() -> AuthBackend:
    return dynamic_import.instantiate(config.api.auth.backend)


@functools.lru_cache()
def get_api_key_signer() -> tokens.Signer:
    return tokens.get_signer(config.api.auth.api_key_signing_secret.get_secret_value())


def _generate_unique_id(route: fastapi.routing.APIRoute) -> str:
    """Strip everything but the function name from the ``operationId``
    fields."""
    return route.name


app = fastapi.FastAPI(
    title="Dyff Auth API",
    generate_unique_id_function=_generate_unique_id,
)

app.add_middleware(
    SessionMiddleware, secret_key=config.api.auth.session_secret.get_secret_value()
)

oauth = OAuth()

# Conditionally register oauth and endpoints for each provider
if config.api.auth.google.client_id and config.api.auth.google.client_secret:
    oauth.register(
        "google",
        client_id=config.api.auth.google.client_id.get_secret_value(),
        client_secret=config.api.auth.google.client_secret.get_secret_value(),
        server_metadata_url=CONF_URL,
        client_kwargs={"scope": "openid email profile"},
    )

    @app.get("/login/google", summary="Initiate Google OAuth 2.0 login flow redirect")
    async def login_google(request: Request):
        redirect_uri = request.url_for("authorize_google")
        return await oauth.google.authorize_redirect(request, redirect_uri)

    @app.get(
        "/authorize/google", summary="Callback endpoint from google OAuth 2.0 flow."
    )
    async def authorize_google(
        request: Request, auth_backend: AuthBackend = Depends(get_auth_backend)
    ):
        loop = asyncio.get_event_loop()
        try:
            token = await oauth.google.authorize_access_token(request)
        except MismatchingStateError:
            # Re redirect if bad google state, stale cookies
            redirect_uri = request.url_for("authorize_google")
            return await oauth.google.authorize_redirect(request, redirect_uri)

        # Ignore google access_token - not needed for subsequent google API calls
        google_jwt: dict = token["userinfo"]

        try:
            google_uid = google_jwt["sub"]
            google_username = google_jwt["email"]
        except KeyError:
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_401_UNAUTHORIZED,
                detail="Invalid google token information",
            )

        google_identity = Identity(google=google_uid)
        get_acccount_partial = functools.partial(
            auth_backend.get_account, identity=google_identity
        )
        account = await loop.run_in_executor(None, get_acccount_partial)

        # If no backend account is found, create a new one
        if account is None:
            create_base_user_partial = functools.partial(
                create_base_user,
                auth_backend=auth_backend,
                username=google_username,
                identity=google_identity,
            )
            account = await loop.run_in_executor(None, create_base_user_partial)

        if account.id is None:
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="account.id is None",
            )

        keys = account.apiKeys
        if len(keys) != 1:
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Number of API Keys on account was not 1. Bad acc creation flow",
            )

        # TODO: DYFF-261 We're using the (singleton) API key associated with
        # the Account to store the Account's permissions. The permissions
        # should be pulled up into Account itself, or into a RoleBinding type.
        # See: https://dsri-org.atlassian.net/wiki/x/KYAYBw
        account_key = keys[0]

        session_key = tokens.generate_api_key(
            subject_type=Entities.Account,
            subject_id=account.id,
            grants=account_key.grants,
            # TODO: Configure default expiration delta
            # TODO: DYFF-524 Use a shorter expiration with "refresh" tokens
            expires=timestamp.now() + timedelta(hours=6),
        )

        session_token = get_api_key_signer().sign_api_key(session_key)

        callback_url = config.api.auth.frontend_callback_url
        query_params = urlencode({"token": session_token})
        redirect_link = f"{callback_url}?{query_params}"

        # TODO: Only returns Dyff JWT token, expand user schema in future
        return RedirectResponse(redirect_link, status_code=status.HTTP_302_FOUND)

    @app.get(
        "/login/anonymous",
        summary="Login anonymously by returning an API Key with public read only permissions",
    )
    async def login_anonymous(request: Request):
        anonymous_token = create_anonymous_session_token()

        # Allow custom redirect path after hitting frontend callback
        redirectPath = request.query_params.get("redirect")
        query = {"token": anonymous_token}
        if redirectPath:
            query["redirect"] = redirectPath

        callback_url = config.api.auth.frontend_callback_url
        redirect_link = f"{callback_url}?{urlencode(query)}"
        return RedirectResponse(redirect_link, status_code=status.HTTP_302_FOUND)


# Yanked from mgmt/tokens.py - Separate implementation without click
def create_base_user(
    auth_backend: AuthBackend, username: str, identity: Identity
) -> Account:
    new_account = auth_backend.create_account(username, identity)
    assert new_account.id is not None

    role = roles.audit_developer(new_account.id)
    grants = role["grants"]

    api_key = tokens.generate_api_key(
        subject_type=Entities.Account,
        subject_id=new_account.id,
        grants=grants,
        generate_secret=True,
    )

    hashed_key = tokens.hashed_api_key(api_key)

    # TODO - Allow managing multiple api keys - don't revoke all?
    auth_backend.revoke_all_api_keys(new_account.id)
    auth_backend.add_api_key(new_account.id, hashed_key)

    updated_account = auth_backend.get_account(id=new_account.id)
    assert updated_account is not None
    return updated_account


# Create API Key that only has permissions that can GET resources only in public account
def create_anonymous_session_token() -> str:
    expires = timestamp.now() + timedelta(hours=1)
    grant = AccessGrant(
        resources=[Resources.ALL],
        functions=[APIFunctions.get, APIFunctions.query, APIFunctions.data],
        accounts=["public"],
    )
    session_key = tokens.generate_api_key(
        subject_type=Entities.Account,
        subject_id="public",
        grants=[grant],
        expires=expires,
    )
    return get_api_key_signer().sign_api_key(session_key)
