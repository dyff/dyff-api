# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0
"""FastAPI definition of the alignmentlabs.dyff Web API.

Naming conventions
------------------

Endpoint functions must be named like ``category_operation()``, e.g.,
``reports_create()``, and the name must be **globally unique**, even if
the functions are defined in different namespaces. We rely on this format
in two places:

#. The autorest code generator exposes the functions like
``DyffAPI.reports.create()``, where the names are determined by splitting
the endpoint function name on underscores. To facilitate this, we strip all of
the unique-ifying information from the OpenAPI ``operationId`` fields generated
by FastAPI, leaving only the function name (that's why it must be unique).
#. API keys grant access to specific resources and functions on those
resources. We determine which resource and function is being requested
by, again, splitting the endpoint function name on underscores.
"""

# mypy: disable-error-code="import-untyped"

# Note: This breaks fastapi, with a NameError while resolving forward refs.
# The underlying issue is that the Endpoint class has a forward reference
# in one of its functions, and Endpoint is used in a fastapi Depends() context.
# from __future__ import annotations

import base64
import functools
import io
import json
import tarfile
from datetime import datetime, timedelta, timezone
from pathlib import Path
from typing import (
    IO,
    Any,
    Callable,
    Container,
    Iterable,
    NamedTuple,
    Optional,
    TypeVar,
    Union,
)

import fastapi
import fastapi.encoders
import fastapi.security
import gitlab
import httpx
import starlette.background
from fastapi.security.utils import get_authorization_scheme_param

from dyff import storage
from dyff.api import dynamic_import, timestamp, tokens
from dyff.api.config import config
from dyff.schema import commands, errors, ids
from dyff.schema.base import DyffBaseModel, Null
from dyff.schema.commands import EntityIdentifier, FamilyIdentifier
from dyff.schema.platform import (  # Role,
    AccessGrant,
    AnalysisData,
    AnalysisScope,
    APIFunctions,
    APIKey,
    ArtifactURL,
    Dataset,
    DatasetStatus,
    Documentation,
    DyffEntity,
    DyffEntityType,
    DyffModelWithID,
    DyffSchemaBaseModel,
    Entities,
    EntityStatus,
    EntityStatusReason,
    Evaluation,
    Family,
    FamilyMember,
    ForeignInferenceService,
    ForeignMethod,
    ForeignModel,
    InferenceService,
    InferenceSession,
    InferenceSessionAndToken,
    InferenceSessionSpec,
    Measurement,
    Method,
    MethodInputKind,
    MethodOutputKind,
    MethodScope,
    Model,
    ModelStorageMedium,
    Module,
    Report,
    Resources,
    Revision,
    SafetyCase,
    Status,
    StorageSignedURL,
    UseCase,
    is_status_success,
    is_status_terminal,
)
from dyff.schema.requests import (
    AnalysisCreateRequest,
    ConcernCreateRequest,
    DatasetCreateRequest,
    DatasetQueryRequest,
    DocumentationEditRequest,
    DocumentationQueryRequest,
    EvaluationCreateRequest,
    EvaluationQueryRequest,
    FamilyCreateRequest,
    FamilyMembersEditRequest,
    FamilyQueryRequest,
    InferenceServiceCreateRequest,
    InferenceServiceQueryRequest,
    InferenceSessionCreateRequest,
    InferenceSessionQueryRequest,
    InferenceSessionTokenCreateRequest,
    LabelsEditRequest,
    MeasurementQueryRequest,
    MethodCreateRequest,
    MethodQueryRequest,
    ModelCreateRequest,
    ModelQueryRequest,
    ModuleCreateRequest,
    ModuleQueryRequest,
    ReportCreateRequest,
    ReportQueryRequest,
    SafetyCaseQueryRequest,
    UseCaseQueryRequest,
)
from dyff.storage.backend.base.auth import AuthBackend
from dyff.storage.backend.base.command import CommandBackend
from dyff.storage.backend.base.query import QueryBackend, Whitelist

from . import public
from ._version import __version__


def _initial_creationTime() -> datetime:
    return timestamp.now()


def _initial_status() -> str:
    return EntityStatus.created


def _set_system_fields(entity_dict: dict) -> None:
    entity_dict["id"] = ids.generate_entity_id()
    entity_dict["creationTime"] = _initial_creationTime()
    entity_dict["status"] = _initial_status()


def _namespaced_id(e: DyffEntity) -> str:
    return f"{Resources.for_kind(Entities(e.kind)).value}/{e.id}"


def _stream_from_storage(path: str) -> fastapi.responses.StreamingResponse:
    def iterfile():
        # TODO: Actually stream multi-part download instead of downloading the
        # whole thing and "streaming" from memory
        data = storage.get_object(path)
        yield from data

    return fastapi.responses.StreamingResponse(iterfile())


def _stream_from_fileobj(fileobj: IO[bytes]) -> fastapi.responses.StreamingResponse:
    def iterfile(fileobj=fileobj):
        while data := fileobj.read(1024):
            yield data

    return fastapi.responses.StreamingResponse(iterfile())


class AutoRestAPIKeyHeader(fastapi.security.APIKeyHeader):
    """This functions the same way as ``OAuth2PasswordBearer``, but with
    ``scheme_name`` set to ``"AzureKey"``.

    For use with APIs for which we will generate client libraries using
    the autorest tool.
    """

    def __init__(self, **kwargs):
        super().__init__(
            name="Authorization",
            scheme_name="AzureKey",
            description="API key authorization",
            **kwargs,
        )

    async def __call__(self, request: fastapi.Request) -> Optional[str]:
        authorization = request.headers.get("Authorization")
        scheme, param = get_authorization_scheme_param(authorization)
        if not authorization or scheme.lower() != "bearer":
            if self.auto_error:
                raise fastapi.HTTPException(
                    status_code=fastapi.status.HTTP_401_UNAUTHORIZED,
                    detail="Not authenticated",
                    headers={"WWW-Authenticate": "Bearer"},
                )
            else:
                return None  # pragma: nocover
        return param


# ---------------------------------------------------------------------------


@functools.lru_cache()
def get_command_backend() -> CommandBackend:
    return dynamic_import.instantiate(config.api.command.backend)


@functools.lru_cache()
def get_query_backend() -> QueryBackend:
    return dynamic_import.instantiate(config.api.query.backend)


@functools.lru_cache()
def get_auth_backend() -> AuthBackend:
    return dynamic_import.instantiate(config.api.auth.backend)


@functools.lru_cache()
def get_gitlab_client() -> gitlab.Gitlab:
    return gitlab.Gitlab(
        private_token=config.gitlab.audit_reader_access_token.get_secret_value()
    )


@functools.lru_cache()
def get_security_scheme(auto_error: bool = True) -> AutoRestAPIKeyHeader:
    return AutoRestAPIKeyHeader(auto_error=auto_error)


@functools.lru_cache()
def get_api_key_signer() -> tokens.Signer:
    return tokens.get_signer(config.api.auth.api_key_signing_secret.get_secret_value())


# FIXME: We can't close the client because the FastAPI lifespan hooks only
# execute for the main server, and the API part is a sub-application. In the
# long run, we should separate the API server from the web content server so
# that they can be scaled independently.
# (It doesn't actually matter if we close the client because it only happens
# on application termination anyway :)
@functools.lru_cache()
def get_inference_async_client() -> httpx.AsyncClient:
    # Inference might take a (very) long time
    # TODO: Make configurable
    return httpx.AsyncClient(timeout=httpx.Timeout(5, read=None))


# ---------------------------------------------------------------------------


def archive_gitlab_directory(remote_path: Path, *, ref: str = "HEAD") -> io.BytesIO:
    def _file_size(f):
        f.seek(0, 2)
        file_size = f.tell()
        f.seek(0)
        return file_size

    project = get_gitlab_client().projects.get(
        config.storage.audit_leaderboards_gitlab_project
    )
    buffer = io.BytesIO()
    with tarfile.open(fileobj=buffer, mode="w:gz") as tgz:
        # The Gitlab API doesn't seem to distinguish between "directory empty" and
        # "directory not present". We're going to interpret "empty" as "missing"
        # and raise a 404.
        empty = True
        for d in project.repository_tree(path=str(remote_path), recursive=True):
            if d["type"] != "blob":
                continue

            empty = False
            remote_file = d["path"]
            # Strip directory prefix from remote path
            relative_file_path = Path(remote_file).relative_to(remote_path)

            blob = io.BytesIO()
            project.files.raw(remote_file, ref=ref, streamed=True, action=blob.write)

            info = tarfile.TarInfo(str(relative_file_path))
            info.size = _file_size(blob)
            tgz.addfile(info, blob)
        if empty:
            raise fastapi.HTTPException(status_code=fastapi.status.HTTP_404_NOT_FOUND)
    buffer.seek(0)
    return buffer


class Endpoint(NamedTuple):
    resource: str
    function: str

    @staticmethod
    def from_funcname(funcname: str) -> "Endpoint":
        resource, function = funcname.split("_", maxsplit=1)
        return Endpoint(resource=resource, function=function)


def get_endpoint(request: fastapi.Request) -> Endpoint:
    # This gets the Python function name that implements the endpoint. This is
    # the simplest way to determine which operation was requested (else we would
    # have to look at both the URL format and the method), and our names are
    # already constrained to be like 'resource_function' by the AutoRest tool.
    funcname = request.scope["route"].name
    return Endpoint.from_funcname(funcname)


def in_star(query: Optional[str], grant: Container[str]) -> bool:
    if query is None:
        return False
    return ("*" in grant) or (query in grant)


def verify_api_key(
    token: str,
    auth_backend: AuthBackend,
) -> APIKey:
    """Unpacks an API token into an APIKey object.

    Raises ``HTTPException(401)``
    if ``token`` is invalid.
    """
    try:
        return tokens.verify_api_token(
            token, get_api_key_signer(), auth_backend=auth_backend
        )
    except tokens.AuthenticationError as ex:
        # FIXME: See comment in alignmentlabs.dyff.web.server
        # logging.getLogger("api-server").exception("unauthorized")
        raise fastapi.HTTPException(
            fastapi.status.HTTP_401_UNAUTHORIZED,
            detail=str(ex),
            headers={"WWW-Authenticate": "Bearer"},
        )


# Note: Actually call get_security_scheme() because the dependency is the thing
# that it returns.
def verified_api_key(
    token: str = fastapi.Depends(get_security_scheme()),
    auth_backend: AuthBackend = fastapi.Depends(get_auth_backend),
) -> APIKey:
    """FastAPI dependency that calls verify_api_key().

    Raises ``HTTPException(401)``
    if ``token`` is invalid.
    """
    return verify_api_key(token, auth_backend)


def check_route_permissions(
    request: fastapi.Request, api_key: APIKey = fastapi.Depends(verified_api_key)
) -> None:
    """Checks permissions that can be checked when we only know the endpoint
    being called.

    This avoids making a DB query if the caller doesn't have
    permission for the endpoint. Raises ``HTTPException(401)`` if ``token`` is
    invalid, or ``HTTPException(403)`` if the token is valid but doesn't grant
    access to the requested endpoint.
    """
    # This gets the Python function name that implements the endpoint. This is
    # the simplest way to determine which operation was requested (else we would
    # have to look at both the URL format and the method), and our names are
    # already constrained to be like 'resource_function' by the AutoRest tool.
    path = request.scope["route"].name
    resource, function = path.split("_", maxsplit=1)

    for grant in api_key.grants:
        if (
            in_star(resource, grant.resources)
            # TODO: Can't currently check 'function' because APIFunctions
            # no longer corresponds 1-to-1 with endpoint names
            # and in_star(function, grant.functions)
            and (len(grant.accounts) > 0 or len(grant.entities) > 0)
        ):
            break
    else:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_403_FORBIDDEN,
            detail="API key does not grant access to this endpoint",
            headers={"WWW-Authenticate": "Bearer"},
        )


# New approach:
# 1. Build list of all accounts and entities that are granted for current resource + function
# 2. Create query constraints to limit results to those resources
# 3. Intersect user query with query constraints
# 3a. Short-circuit queries where we know the result set will be empty (e.g., user wants '?account=private' but doesn't have permission)


def is_endpoint_allowed(
    api_key: APIKey, endpoint: Endpoint, *, account: Optional[str], id: Optional[str]
) -> bool:
    function = endpoint.function
    if function in ["label", "edit_documentation", "edit_members"]:
        function = "edit"
    for grant in api_key.grants:
        if (
            in_star(endpoint.resource, grant.resources)
            and in_star(function, grant.functions)
            and (in_star(account, grant.accounts) or in_star(id, grant.entities))
        ):
            return True
    return False


def filter_forbidden_entities(
    api_key: APIKey, endpoint: Endpoint, entities: Iterable[DyffEntity]
) -> Iterable[DyffEntity]:
    """Filters a stream of ``DyffEntity`` objects to exclude entities that the
    provided ``APIKey`` does not grant permission to access."""
    for entity in entities:
        if is_endpoint_allowed(api_key, endpoint, account=entity.account, id=entity.id):
            yield entity


def check_endpoint_permissions(
    api_key: APIKey, endpoint: Endpoint, *, account: Optional[str], id: Optional[str]
):
    if not is_endpoint_allowed(api_key, endpoint, account=account, id=id):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_403_FORBIDDEN,
            detail="API key does not grant access to this endpoint",
            headers={"WWW-Authenticate": "Bearer"},
        )


def check_endpoint_permissions_for_entity(
    api_key: APIKey, endpoint: Endpoint, entity: DyffModelWithID
):
    check_endpoint_permissions(api_key, endpoint, account=entity.account, id=entity.id)


def build_whitelist(api_key: APIKey, endpoint: Endpoint) -> Whitelist:
    accounts = set()
    entities = set()

    for grant in api_key.grants:
        if in_star(endpoint.resource, grant.resources) and in_star(
            endpoint.function, grant.functions
        ):
            accounts.update(grant.accounts)
            entities.update(grant.entities)

    return Whitelist(accounts=accounts, entities=entities)


_DyffEntityT = TypeVar("_DyffEntityT", bound=DyffEntity)


def require_entity_available(
    kind: Entities, id: str, entity: Optional[_DyffEntityT]
) -> _DyffEntityT:
    if entity is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND,
            f"referenced {kind} {id} not found",
        )
    # Sessions are unavailable if they've terminated
    elif is_status_terminal(entity.status) and (
        kind == Entities.InferenceSession or not is_status_success(entity.status)
    ):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"referenced {kind} {id} unavailable: {entity.status} ({entity.reason})",
        )
    return entity


def validate_analysis_scope(query_backend: QueryBackend, scope: AnalysisScope) -> None:
    if scope.dataset is not None and query_backend.get_dataset(scope.dataset) is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"scope.dataset: {scope.dataset}"
        )
    if (
        scope.evaluation is not None
        and query_backend.get_evaluation(scope.evaluation) is None
    ):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"scope.evaluation: {scope.evaluation}"
        )
    if (
        scope.inferenceService is not None
        and query_backend.get_inference_service(scope.inferenceService) is None
    ):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND,
            f"scope.inferenceService: {scope.inferenceService}",
        )
    if scope.model is not None and query_backend.get_model(scope.model) is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"scope.model: {scope.model}"
        )


_UpcastTargetT = TypeVar("_UpcastTargetT", bound=DyffBaseModel)
_UpcastSourceT = TypeVar("_UpcastSourceT", bound=DyffBaseModel)


def upcast(
    t: type[_UpcastTargetT], obj: Union[_UpcastSourceT, dict[str, Any]]
) -> _UpcastTargetT:
    if not isinstance(obj, dict):
        # Preserve the unset status
        obj = obj.dict(exclude_unset=True)
    fields = {k: v for k, v in obj.items() if k in t.__fields__}
    return t.parse_obj(fields)


# ----------------------------------------------------------------------------


def _generate_unique_id(route: fastapi.routing.APIRoute) -> str:
    """Strip everything but the function name from the ``operationId``
    fields."""
    return route.name


# See: https://github.com/tiangolo/fastapi/discussions/6695#discussioncomment-7251043
def customize_openapi(func: Callable[..., dict]) -> Callable[..., dict]:
    """Remove 422 response schemas from the OpenAPI spec.

    This prevents autorest from thinking they are "normal" responses and
    returning the JSON body instead of throwing an exception.
    """

    def wrapper(*args, **kwargs) -> dict:
        """Wrapper."""
        res = func(*args, **kwargs)
        for _, method_item in res.get("paths", {}).items():
            for _, param in method_item.items():
                responses = param.get("responses")
                # remove default 422 - the default 422 schema is HTTPValidationError
                if "422" in responses and responses["422"]["content"][
                    "application/json"
                ]["schema"]["$ref"].endswith("HTTPValidationError"):
                    del responses["422"]
        return res

    return wrapper


app = fastapi.FastAPI(
    dependencies=[fastapi.Depends(check_route_permissions)],
    generate_unique_id_function=_generate_unique_id,
    redoc_url=None,
    title="Dyff v0 API",
    version=__version__,
)
# See: FastAPI.__init__
# autorest only accepts 3.0.x. FastAPI actually uses 3.1.x, but if we avoid
# 3.1-exclusive features, it still works.
app.openapi_version = "3.0.2"
# Fix some issues with OpenAPI spec that prevent proper client code generation
setattr(app, "openapi", customize_openapi(app.openapi))

# Have to mount here and not in server.py because we want it at /v0/public
# Note that sub-mounts don't seem to inherit dependencies / etc. from parent
app.mount("/public", public.app)


@app.exception_handler(errors.ClientError)
async def dyff_client_error_handler(request, exc: errors.ClientError):
    """Automatically translate ClientError to an HTTP 4xx status."""
    http_status = exc.http_status or errors.HTTP_400_BAD_REQUEST
    # This is a combination of json-api recommendations and fields that
    # autorest-generated client code seems to understand (i.e., 'message')
    # See: https://jsonapi.org/format/#error-objects
    return fastapi.responses.JSONResponse(
        status_code=http_status.code,
        content={
            "code": http_status.code,
            "status": http_status.status,
            "detail": exc.message,
            "message": f"{http_status.status} -- {exc.message}",
        },
    )


@app.exception_handler(errors.PlatformError)
async def dyff_platform_error_handler(request, exc: errors.PlatformError):
    """Automatically translate PlatformError to an HTTP 5xx status."""
    http_status = exc.http_status or errors.HTTP_500_INTERNAL_SERVER_ERROR
    # This is a combination of json-api recommendations and fields that
    # autorest-generated client code seems to understand (i.e., 'message')
    # See: https://jsonapi.org/format/#error-objects
    return fastapi.responses.JSONResponse(
        status_code=http_status.code,
        content={
            "code": http_status.code,
            "status": http_status.status,
            "detail": exc.message,
            "message": f"{http_status.status} -- {exc.message}",
        },
    )


@app.exception_handler(fastapi.exceptions.RequestValidationError)
async def request_validation_exception_handler(
    request, exc: fastapi.exceptions.RequestValidationError
):
    """Override validation error handler to use code 400 and follow the same
    format as other errors."""
    http_status = errors.HTTP_400_BAD_REQUEST
    validation_errors = fastapi.encoders.jsonable_encoder(exc.errors())
    # This is a combination of json-api recommendations and fields that
    # autorest-generated client code seems to understand (i.e., 'message')
    # See: https://jsonapi.org/format/#error-objects
    return fastapi.responses.JSONResponse(
        status_code=http_status.code,
        content={
            "code": http_status.code,
            "status": http_status.status,
            "detail": validation_errors,
            "message": validation_errors,
        },
    )


# ----------------------------------------------------------------------------


@app.get(
    "/tokens/current",
    tags=["tokens"],
    summary="Verify the signature of the provied token and return its JSON body.",
    response_description="The JSON body of the provided token.",
)
def tokens_current(
    api_key: APIKey = fastapi.Depends(verified_api_key),
) -> APIKey:
    api_key.secret = None
    return api_key


# @app.post(
#     "/tokens",
#     tags=["tokens"],
#     summary="Create a new API token with the specified permissions.",
#     response_description="A signed API token.",
# )
# def tokens_create(
#     role: Optional[Role] = None,
#     api_key: APIKey = fastapi.Depends(verified_api_key),
# ) -> str:
#     if role is None:
#         role = Role(grants=api_key.grants)
#     token_role = Role(grants=api_key.grants)
#     if not role.is_subset_of(token_role):
#         raise fastapi.HTTPException(
#             fastapi.status.HTTP_403_FORBIDDEN,
#             detail="Privilege escalation",
#             headers={"WWW-Authenticate": "Bearer"},
#         )

#     new_key = tokens.generate_api_key(
#         subject_type=Entities.Account,
#         subject_id=account.id,
#         grants=role.grants,
#         # TODO: Configure default expiration delta
#         # TODO: DYFF-524 Use a shorter expiration with "refresh" tokens
#         expires=timestamp.now() + timedelta(hours=6),
#     )

#     return get_api_key_signer().sign_api_key(new_key)


# ----------------------------------------------------------------------------


# @app.post(
#     f"/{Resources.Dataset}/{Resources.Family}",
#     tags=[Resources.Dataset],
#     summary="Create a Dataset Family.",
# )
# def datasets_create_family(
#     query_backend: QueryBackend = fastapi.Depends(get_query_backend),
#     api_key: APIKey = fastapi.Depends(verified_api_key),
#     endpoint: Endpoint = fastapi.Depends(get_endpoint),
# ) -> Dataset:
#     """Get a Dataset Family by its key. Raises a 404 error if no entity exists
#     with that key.
#     """
#     family = query_backend.get_family(family_id)
#     if family is None or family.resourceKind != Entities.Dataset:
#         raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
#     check_endpoint_permissions_for_entity(
#         api_key, Endpoint(Resources.Dataset.value, APIFunctions.get.value), family
#     )
#     return family


# @app.get(
#     f"/{Resources.Dataset}/{Resources.Family}/{{family_id}}",
#     tags=[Resources.Dataset],
#     summary="Get a Dataset Family by its ID.",
# )
# def datasets_family(
#     family_id: str,
#     query_backend: QueryBackend = fastapi.Depends(get_query_backend),
#     api_key: APIKey = fastapi.Depends(verified_api_key),
#     endpoint: Endpoint = fastapi.Depends(get_endpoint),
# ) -> Dataset:
#     """Get a Dataset Family by its key. Raises a 404 error if no entity exists
#     with that key.
#     """
#     family = query_backend.get_family(family_id)
#     if family is None or family.resourceKind != Entities.Dataset:
#         raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
#     check_endpoint_permissions_for_entity(
#         api_key, Endpoint(Resources.Dataset.value, APIFunctions.get.value), family
#     )
#     return family


# @app.get(
#     f"/{Resources.Dataset}/{Resources.Family}/{{family_id}}/{{tag}}",
#     tags=[Resources.Dataset],
#     summary="Get a Dataset by '<family-id>/<tag>'.",
# )
# def datasets_tagged(
#     family_id: str,
#     tag: str,
#     query_backend: QueryBackend = fastapi.Depends(get_query_backend),
#     api_key: APIKey = fastapi.Depends(verified_api_key),
#     endpoint: Endpoint = fastapi.Depends(get_endpoint),
# ) -> Dataset:
#     """Get a Dataset by '<family-id>/<tag>'. Raises a 404 error if no
#     entity exists with that key.
#     """
#     family = query_backend.get_family(family_id)
#     if family is None or family.resourceKind != Entities.Dataset:
#         raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
#     check_endpoint_permissions_for_entity(
#         api_key, Endpoint(Resources.Dataset.value, APIFunctions.get.value), family
#     )

#     for tag_obj in family.tags:
#         if tag_obj.tag == tag:
#             dataset = query_backend.get_dataset(tag_obj.resource)
#             if dataset is None:
#                 # Invariant: tags should never refer to a non-existent resource
#                 raise fastapi.HTTPException(
#                     fastapi.status.HTTP_500_INTERNAL_SERVER_ERROR,
#                     detail="Tag references non-existent resource",
#                 )
#             check_endpoint_permissions_for_entity(
#                 api_key,
#                 Endpoint(Resources.Dataset.value, APIFunctions.get.value),
#                 dataset,
#             )
#             return dataset
#     raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)


# @app.post(
#     f"/{Resources.Dataset}/{Resources.Family}/{{family_id}}/tags",
#     tags=[Resources.Dataset],
#     summary="Create a new tag in an existing Dataset Family.",
# )
# def datasets_tag(
#     family_id: str,
#     tag_request: TagCreateRequest,
#     query_backend: QueryBackend = fastapi.Depends(get_query_backend),
#     api_key: APIKey = fastapi.Depends(verified_api_key),
#     endpoint: Endpoint = fastapi.Depends(get_endpoint),
# ):
#     family = query_backend.get_family(family_id)
#     if family is None or family.resourceKind != Entities.Dataset:
#         raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
#     check_endpoint_permissions_for_entity(
#         api_key, Endpoint(Resources.Dataset.value, APIFunctions.get.value), family
#     )
#     check_endpoint_permissions_for_entity(
#         api_key, Endpoint(Resources.Dataset.value, APIFunctions.edit.value), family
#     )

#     tag = query_backend.create_tag(tag_request)
#     if tag is None:
#         raise fastapi.HTTPException(
#             fastapi.status.HTTP_409_CONFLICT,
#             detail=f"tag with name {tag_request.tag} already exists",
#         )
#     return tag


@app.patch(
    f"/{Resources.Dataset}/{{id}}/labels",
    tags=[Resources.Dataset],
    summary="Update labels for an existing Dataset.",
)
def datasets_label(
    id: str,
    label_request: LabelsEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
):
    dataset = query_backend.get_dataset(id)
    if dataset is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, dataset)
    return command_backend.edit_entity_labels(
        EntityIdentifier.of(dataset),
        edit=label_request,
    )


@app.get(
    f"/{Resources.Dataset}",
    tags=[Resources.Dataset],
    summary="Get all Datasets matching a query.",
    response_description="The Datasets matching the query.",
)
def datasets_query(
    query: DatasetQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[Dataset]:
    """Get all Datasets matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist = build_whitelist(api_key, endpoint)
    results = query_backend.query_datasets(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Dataset}/{{id}}",
    tags=[Resources.Dataset],
    summary="Get a Dataset by its key.",
    response_description="The Dataset with the given key.",
)
def datasets_get(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Dataset:
    """Get a Dataset by its key.

    Raises a 404 error if no entity exists with that key.
    """
    dataset = query_backend.get_dataset(id)
    if dataset is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, dataset)
    return dataset


# Problem: Since we can't revoke signed URLs, there is a window where the dataset
# could be altered after the initial upload. A malicious user could upload a
# manipulated Dataset, run an Evaluation on the Dataset and get a good result,
# then replace the Dataset with a legitimate one.
#
# Solution: The user specifies the checksum of the data when creating the
# Dataset record. Consumers of the Dataset verify the checksum.
#
# 1. User calls POST /datasets {"artifacts": [Artifact]} and gets a new Dataset record in Created status
#    a. Server creates .dyff/artifacts.json in <bucket>/<id>/
#    b. Server produces Create event
# 2. User calls GET /datasets/<id>/upload/<artifact-name> -> signed upload URL
# 3. User uploads the file using the signed URL
# 4. User calls POST /datasets/<id>/finalize
#    a. Server checks that all artifacts are uploaded and match provided checksum
#    b. Server produces .status event
# 5. Dataset consumers verify artifact digests against the GCS Blob.md5_hash
@app.post(
    f"/{Resources.Dataset}",
    tags=[Resources.Dataset],
    summary="Create a Dataset.",
    response_description="The created Dataset entity.",
)
def datasets_create(
    dataset_request: DatasetCreateRequest,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Dataset:
    # Check permission to create evaluations
    check_endpoint_permissions(
        api_key, endpoint, account=dataset_request.account, id=None
    )
    # FIXME: This is (possibly?) gcloud-specific -- gcloud only provides md5 hashes
    if any(artifact.digest.md5 is None for artifact in dataset_request.artifacts):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            "gcloud storage requires artifact.digest.md5",
        )
    dataset_dict = dataset_request.dict()
    _set_system_fields(dataset_dict)
    dataset = Dataset.parse_obj(dataset_dict)
    # This .reason indicates that the orchestrator shouldn't attempt to
    # schedule the workflow because it's waiting for external input.
    # FIXME: .reason is not for flow control! This should be an annotation or something
    dataset.reason = "WaitingForUpload"
    command_backend.create_entity(dataset)
    return dataset


@app.put(
    f"/{Resources.Dataset}/{{id}}/delete",
    tags=[Resources.Dataset],
    summary="Mark a Dataset for deletion.",
    response_description="The resulting status of the entity.",
)
def datasets_delete(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    dataset = query_backend.get_dataset(id)
    if dataset is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, dataset)
    if dataset.status != EntityStatus.deleted:
        command_backend.delete_entity(EntityIdentifier.of(dataset))
        return Status(
            status=EntityStatus.deleted, reason=EntityStatusReason.delete_command
        )
    else:
        return Status(status=dataset.status, reason=dataset.reason)


@app.get(
    f"/{Resources.Dataset}/{{id}}/upload/{{artifact_path:path}}",
    tags=[Resources.Dataset],
    summary="Get a signed URL to which the given artifact can be uploaded.",
    response_description="A signed upload URL.",
)
def datasets_upload(
    id: str,
    artifact_path: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> StorageSignedURL:
    dataset = query_backend.get_dataset(id)
    if dataset is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no dataset {id}"
        )
    check_endpoint_permissions_for_entity(api_key, endpoint, dataset)
    if dataset.status != DatasetStatus.created:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected dataset.status == {DatasetStatus.created}; got {dataset.status}",
        )
    for artifact in dataset.artifacts:
        if artifact.path == artifact_path:
            break
    else:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT, f"no artifact {artifact} in dataset"
        )
    return storage.signed_url_for_artifact_upload(
        artifact, storage.paths.dataset_root(id)
    )


@app.post(
    f"/{Resources.Dataset}/{{id}}/finalize",
    tags=[Resources.Dataset],
    summary="Indicate that all dataset artifacts have been uploaded.",
    response_description="A signed upload URL.",
)
def datasets_finalize(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> None:
    dataset = query_backend.get_dataset(id)
    if dataset is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no dataset {id}"
        )
    check_endpoint_permissions_for_entity(api_key, endpoint, dataset)

    if dataset.status == DatasetStatus.ready:
        return
    elif dataset.status != DatasetStatus.created:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected dataset.status == {DatasetStatus.created}; got {dataset.status}",
        )

    for artifact in dataset.artifacts:
        if artifact.digest.md5 is None:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_500_INTERNAL_SERVER_ERROR,
                f"artifact.digest.md5 is None; this should have been caught at dataset creation",
            )

        try:
            storage_md5 = storage.artifact_md5hash(
                artifact, storage.paths.dataset_root(id)
            )
        except KeyError:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_409_CONFLICT,
                f"artifact not uploaded: {artifact.path}",
            )

        if storage_md5 != base64.b64decode(artifact.digest.md5):
            raise fastapi.HTTPException(
                fastapi.status.HTTP_409_CONFLICT,
                f"artifact hash mismatch: {artifact.path}",
            )

    artifacts_dict = dataset.dict()["artifacts"]
    artifacts_json = json.dumps(artifacts_dict)
    storage.put_object(
        io.BytesIO(artifacts_json.encode()),
        f"{storage.paths.dataset_root(dataset.id)}/.dyff/artifacts.json",
    )
    command_backend.update_status(
        EntityIdentifier.of(dataset), status=DatasetStatus.ready
    )


@app.get(
    f"/{Resources.Dataset}/{{id}}/downlinks",
    tags=[Resources.Dataset],
    summary="Get a list of signed GET URLs from which Dataset artifacts can be downloaded.",
    response_description="A list of signed GET URLs.",
)
def datasets_downlinks(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[ArtifactURL]:
    dataset = query_backend.get_dataset(id)
    if dataset is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no dataset {id}"
        )

    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.Dataset.value,
            function=APIFunctions.data.value,
        ),
        dataset,
    )

    if dataset.status != EntityStatus.ready:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected dataset.status == {EntityStatus.ready}; got {dataset.status}",
        )

    return list(storage.artifact_downlinks(Entities.Dataset, id))


@app.get(
    f"/{Resources.Dataset}/{{id}}/documentation",
    tags=[Resources.Dataset],
    summary="Get the documentation associated with a Dataset",
    response_description="The documentation for the Dataset",
)
def datasets_documentation(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Documentation:
    """Get the documentation associated with a Dataset.

    Raises a 404 error if no entity exists with that key.
    """
    dataset = query_backend.get_dataset(id)
    if dataset is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(resource=Resources.Dataset.value, function=APIFunctions.get.value),
        dataset,
    )

    documentation = query_backend.get_documentation(id)
    return documentation or Documentation()


@app.patch(
    f"/{Resources.Dataset}/{{id}}/documentation",
    tags=[Resources.Dataset],
    summary="Edit the documentation associated with a Dataset",
)
def datasets_edit_documentation(
    id: str,
    edit_request: DocumentationEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Documentation:
    """Edit the documentation associated with a Dataset.

    Raises a 404 error if no entity exists with that key. Returns the
    modified Documentation.
    """
    dataset = query_backend.get_dataset(id)
    if dataset is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(resource=Resources.Dataset.value, function=APIFunctions.edit.value),
        dataset,
    )

    documentation = query_backend.edit_documentation(id, edit_request)
    return documentation or Documentation()


@app.get(
    f"/{Resources.Dataset}/{Resources.Documentation}",
    tags=[Resources.Dataset],
    summary="Get all Documentation matching a query and that pertains to Datasets",
    response_description="The documentation for the Dataset",
)
def datasets_query_documentation(
    query: DocumentationQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[Documentation]:
    """Get all Documentation matching a query and that pertains to Datasets.

    Currently, only the ``.id`` field is queryable.
    """
    whitelist = build_whitelist(
        api_key, Endpoint(Resources.Dataset.value, APIFunctions.query.value)
    )
    results = query_backend.query_documentation(whitelist, query)
    return list(results)


# ----------------------------------------------------------------------------


@app.patch(
    f"/{Resources.Evaluation}/{{id}}/labels",
    tags=[Resources.Evaluation],
    summary="Update labels for an existing Evaluation.",
)
def evaluations_label(
    id: str,
    label_request: LabelsEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
):
    evaluation = query_backend.get_evaluation(id)
    if evaluation is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, evaluation)
    return command_backend.edit_entity_labels(
        EntityIdentifier.of(evaluation), label_request
    )


@app.get(
    f"/{Resources.Evaluation}",
    tags=[Resources.Evaluation],
    summary="Get all Evaluations matching a query.",
    response_description="The Evaluations matching the query.",
)
def evaluations_query(
    query: EvaluationQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[Evaluation]:
    """Get all Evaluations matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist = build_whitelist(api_key, endpoint)
    results = query_backend.query_evaluations(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Evaluation}/{{id}}",
    tags=[Resources.Evaluation],
    summary="Get an Evaluation by its key.",
    response_description="The Evaluation with the given key.",
)
def evaluations_get(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Evaluation:
    """Get an Evaluation by its key.

    Raises a 404 error if no entity exists with that key.
    """
    evaluation = query_backend.get_evaluation(id)
    if evaluation is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, evaluation)
    return evaluation


@app.post(
    f"/{Resources.Evaluation}",
    tags=[Resources.Evaluation],
    summary="Create an Evaluation.",
    response_description="The created Evaluation entity.",
)
def evaluations_create(
    evaluation_request: EvaluationCreateRequest,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Evaluation:
    # Check permission to create evaluations
    check_endpoint_permissions(
        api_key, endpoint, account=evaluation_request.account, id=None
    )

    # Referenced Dataset
    dataset_id = evaluation_request.dataset
    dataset = query_backend.get_dataset(dataset_id)
    dataset = require_entity_available(Entities.Dataset, dataset_id, dataset)
    # Check permission to consume referenced Dataset
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(resource=Resources.Dataset.value, function=APIFunctions.consume.value),
        dataset,
    )

    evaluation_dict = evaluation_request.dict()
    _set_system_fields(evaluation_dict)

    service_dependency: Optional[DyffModelWithID] = None
    if evaluation_request.inferenceSessionReference is not None:
        session_id = evaluation_request.inferenceSessionReference
        session = query_backend.get_inference_session(session_id)
        session = require_entity_available(
            Entities.InferenceSession, session_id, session
        )
        evaluation_dict["inferenceSession"] = upcast(
            InferenceSessionSpec, session
        ).dict()
        service_dependency = session.inferenceService
    elif evaluation_request.inferenceSession is not None:
        # Referenced InferenceService
        service_id = evaluation_request.inferenceSession.inferenceService
        service = query_backend.get_inference_service(service_id)
        service = require_entity_available(
            Entities.InferenceService, service_id, service
        )
        evaluation_dict["inferenceSession"]["inferenceService"] = upcast(
            ForeignInferenceService, service
        ).dict()
        service_dependency = service
    else:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_400_BAD_REQUEST,
            detail="must specify exactly one of {inferenceSession, inferenceSessionReference}",
        )

    # Check permission to consume referenced InferenceService
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.InferenceService.value,
            function=APIFunctions.consume.value,
        ),
        service_dependency,
    )

    evaluation = Evaluation.parse_obj(evaluation_dict)

    # FIXME: (DYFF-570) Reject .replicas > 1 for ObjectStorage until we
    # implement this feature.
    if (
        evaluation.inferenceSession.inferenceService.model
        and evaluation.inferenceSession.inferenceService.model.storage.medium
        == ModelStorageMedium.ObjectStorage
        and evaluation.inferenceSession.replicas > 1
    ):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            detail=f"(DYFF-570): Models with storage.medium == ObjectStorage"
            " currently do not support .replicas > 1; try again with .replicas = 1",
        )

    command_backend.create_entity(evaluation)
    return evaluation


@app.put(
    f"/{Resources.Evaluation}/{{id}}/delete",
    tags=[Resources.Evaluation],
    summary="Mark an Evaluation for deletion.",
    response_description="The resulting status of the entity.",
)
def evaluations_delete(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    evaluation = query_backend.get_evaluation(id)
    if evaluation is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, evaluation)
    if evaluation.status != EntityStatus.deleted:
        command_backend.delete_entity(EntityIdentifier.of(evaluation))
        return Status(
            status=EntityStatus.deleted, reason=EntityStatusReason.delete_command
        )
    else:
        return Status(status=evaluation.status, reason=evaluation.reason)


@app.get(
    f"/{Resources.Evaluation}/{{id}}/downlinks",
    tags=[Resources.Evaluation],
    summary="Get a list of signed GET URLs from which Evaluation artifacts can be downloaded.",
    response_description="A list of signed GET URLs.",
)
def evaluations_downlinks(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[ArtifactURL]:
    evaluation = query_backend.get_evaluation(id)
    if evaluation is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no evaluation {id}"
        )

    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.Evaluation.value,
            function=APIFunctions.data.value,
        ),
        evaluation,
    )

    if evaluation.status != EntityStatus.complete:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected evaluation.status == {EntityStatus.complete}; got {evaluation.status}",
        )

    return list(storage.artifact_downlinks(Entities.Evaluation, id))


# ----------------------------------------------------------------------------


@app.patch(
    f"/{Resources.Family}/{{id}}/labels",
    tags=[Resources.Family],
    summary="Update labels for an existing entity.",
)
def families_label(
    id: str,
    label_request: LabelsEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
):
    family = query_backend.get_family(id)
    if family is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, family)
    return command_backend.edit_entity_labels(
        EntityIdentifier.of(family), label_request
    )


@app.get(
    f"/{Resources.Family}",
    tags=[Resources.Family],
    summary="Get all entities matching a query.",
    response_description="The entities matching the query.",
)
def families_query(
    query: FamilyQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[Family]:
    """Get all Families matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist = build_whitelist(api_key, endpoint)
    results = query_backend.query_families(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Family}/{{id}}",
    tags=[Resources.Family],
    summary="Get a Family by its ID.",
    response_description="The Family with the given ID.",
)
def families_get(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Family:
    family = query_backend.get_family(id)
    if family is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, family)
    return family


@app.post(
    f"/{Resources.Family}",
    tags=[Resources.Family],
    summary="Create a Family.",
    response_description="The created Family entity.",
)
def families_create(
    family_request: FamilyCreateRequest,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Family:
    # Check permission to create evaluations
    check_endpoint_permissions(
        api_key, endpoint, account=family_request.account, id=None
    )

    family_dict = family_request.dict()
    _set_system_fields(family_dict)
    family = Family.parse_obj(family_dict)
    command_backend.create_entity(family)
    return family


@app.patch(
    f"/{Resources.Family}/{{id}}/members",
    tags=[Resources.Family],
    summary="Edit the members mapping of a Family.",
)
def families_edit_members(
    id: str,
    members_edit_request: FamilyMembersEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> None:
    family = query_backend.get_family(id)
    if family is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(Resources.Family, APIFunctions.edit),
        family,
    )
    members: dict[str, Optional[FamilyMember]] = {}
    for k, v in members_edit_request.members.items():
        if v is None or isinstance(v, Null):
            members[k] = None
        elif v.entity.kind != family.memberKind:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_400_BAD_REQUEST,
                f"family.memberKind={family.memberKind} but member.entity={v.entity.dict()}",
            )
        else:
            members[k] = FamilyMember(
                entity=v.entity,
                description=v.description,
                name=k,
                family=id,
                creationTime=timestamp.now(),
            )
    return command_backend.edit_family_members(
        FamilyIdentifier(id=id),
        commands.EditFamilyMembersAttributes(members=members),  # type: ignore
    )


@app.patch(
    f"/{Resources.Family}/{{id}}/documentation",
    tags=[Resources.Family],
    summary="Edit the documentation associated with an entity",
)
def families_edit_documentation(
    id: str,
    edit_request: DocumentationEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> None:
    family = query_backend.get_family(id)
    if family is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, family)
    edit = upcast(commands.EditEntityDocumentationPatch, edit_request.documentation)
    command_backend.edit_entity_documentation(EntityIdentifier.of(family), edit)


@app.put(
    f"/{Resources.Family}/{{id}}/delete",
    tags=[Resources.Family],
    summary="Mark a Family for deletion.",
    response_description="The resulting status of the entity.",
)
def families_delete(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    family = query_backend.get_family(id)
    if family is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, family)
    if family.status != EntityStatus.deleted:
        command_backend.delete_entity(EntityIdentifier.of(family))
        return Status(
            status=EntityStatus.deleted, reason=EntityStatusReason.delete_command
        )
    else:
        return Status(status=family.status, reason=family.reason)


# ----------------------------------------------------------------------------


# @app.get(
#     f"/{Resources.History}/{{history_id}}",
#     tags=[Resources.History],
#     summary="Get a History by its key.",
#     response_description="The History with the given key.",
# )
# def histories_get(
#     history_id: str,
#     query_backend: QueryBackend = fastapi.Depends(get_query_backend),
#     api_key: APIKey = fastapi.Depends(verified_api_key),
#     endpoint: Endpoint = fastapi.Depends(get_endpoint),
# ) -> History:
#     """Get a History by its key.

#     Raises a 404 error if no entity exists with that key.
#     """
#     raise NotImplementedError()
#     # history = query_backend.get_history(history_id)
#     # if history is None:
#     #     raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
#     # check_endpoint_permissions_for_entity(api_key, endpoint, history)
#     # return history


# ----------------------------------------------------------------------------


@app.patch(
    f"/{Resources.InferenceService}/{{id}}/labels",
    tags=[Resources.InferenceService],
    summary="Update labels for an existing InferenceService.",
)
def inferenceservices_label(
    id: str,
    label_request: LabelsEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
):
    service = query_backend.get_inference_service(id)
    if service is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, service)
    return command_backend.edit_entity_labels(
        EntityIdentifier.of(service), label_request
    )


@app.get(
    f"/{Resources.InferenceService}",
    tags=[Resources.InferenceService],
    summary="Get all InferenceServices matching a query.",
    response_description="The InferenceServices matching the query.",
)
def inferenceservices_query(
    query: InferenceServiceQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[InferenceService]:
    """Get all InferenceServices matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist = build_whitelist(api_key, endpoint)
    results = query_backend.query_inference_services(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.InferenceService}/{{id}}",
    tags=[Resources.InferenceService],
    summary="Get an InferenceService by its key.",
    response_description="The InferenceService with the given key.",
)
def inferenceservices_get(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> InferenceService:
    """Get an InferenceService by its key.

    Raises a 404 error if no entity exists with that key.
    """
    inferenceservice = query_backend.get_inference_service(id)
    if inferenceservice is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, inferenceservice)
    return inferenceservice


@app.post(
    f"/{Resources.InferenceService}",
    tags=[Resources.InferenceService],
    summary="Create an InferenceService.",
    response_description="The created InferenceService entity.",
)
def inferenceservices_create(
    inference_service_request: InferenceServiceCreateRequest,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> InferenceService:
    # Check permission to create InferenceSession
    check_endpoint_permissions(
        api_key, endpoint, account=inference_service_request.account, id=None
    )
    # Referenced Model
    if inference_service_request.model is not None:
        model_id = inference_service_request.model
        model = query_backend.get_model(model_id)
        model = require_entity_available(Entities.Model, model_id, model)
        # Check permission to consume referenced Model
        check_endpoint_permissions_for_entity(
            api_key,
            Endpoint(
                resource=Resources.Model.value, function=APIFunctions.consume.value
            ),
            model,
        )
    else:
        model = None

    service_dict = inference_service_request.dict()
    _set_system_fields(service_dict)
    service_dict["model"] = upcast(ForeignModel, model).dict() if model else None
    inference_service = InferenceService.parse_obj(service_dict)
    command_backend.create_entity(inference_service)
    return inference_service


@app.put(
    f"/{Resources.InferenceService}/{{id}}/delete",
    tags=[Resources.InferenceService],
    summary="Mark an InferenceService for deletion.",
    response_description="The resulting status of the entity.",
)
def inferenceservices_delete(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    service = query_backend.get_inference_service(id)
    if service is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, service)
    if service.status != EntityStatus.deleted:
        command_backend.delete_entity(EntityIdentifier.of(service))
        return Status(
            status=EntityStatus.deleted, reason=EntityStatusReason.delete_command
        )
    else:
        return Status(status=service.status, reason=service.reason)


@app.get(
    f"/{Resources.InferenceService}/{{id}}/documentation",
    tags=[Resources.InferenceService],
    summary="Get the documentation associated with an InferenceService",
    response_description="The documentation for the InferenceService",
)
def inferenceservices_documentation(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Documentation:
    """Get the documentation associated with an InferenceService.

    Raises a 404 error if no entity exists with that key.
    """
    service = query_backend.get_inference_service(id)
    if service is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.InferenceService.value, function=APIFunctions.get.value
        ),
        service,
    )

    documentation = query_backend.get_documentation(id)
    return documentation or Documentation()


@app.patch(
    f"/{Resources.InferenceService}/{{id}}/documentation",
    tags=[Resources.InferenceService],
    summary="Edit the documentation associated with an InferenceService",
)
def inferenceservices_edit_documentation(
    id: str,
    edit_request: DocumentationEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Documentation:
    """Edit the documentation associated with an InferenceService.

    Raises a 404 error if no entity exists with that key. Returns the
    modified Documentation.
    """
    service = query_backend.get_inference_service(id)
    if service is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.InferenceService.value, function=APIFunctions.edit.value
        ),
        service,
    )

    documentation = query_backend.edit_documentation(id, edit_request)
    return documentation or Documentation()


# ----------------------------------------------------------------------------


@app.patch(
    f"/{Resources.InferenceSession}/{{id}}/labels",
    tags=[Resources.InferenceSession],
    summary="Update labels for an existing InferenceSession.",
)
def inferencesessions_label(
    id: str,
    label_request: LabelsEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
):
    session = query_backend.get_inference_session(id)
    if session is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, session)
    return command_backend.edit_entity_labels(
        EntityIdentifier.of(session), label_request
    )


@app.get(
    f"/{Resources.InferenceSession}",
    tags=[Resources.InferenceSession],
    summary="Get all InferenceSessions matching a query.",
    response_description="The InferenceSessions matching the query.",
)
def inferencesessions_query(
    query: InferenceSessionQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[InferenceSession]:
    """Get all InferenceSessions matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist = build_whitelist(api_key, endpoint)
    results = query_backend.query_inference_sessions(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.InferenceSession}/{{id}}",
    tags=[Resources.InferenceSession],
    summary="Get an InferenceSession by its key.",
    response_description="The InferenceSession with the given key.",
)
def inferencesessions_get(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> InferenceSession:
    """Get an InferenceSession by its key.

    Raises a 404 error if no entity exists with that key.
    """
    inference_session = query_backend.get_inference_session(id)
    if inference_session is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, inference_session)
    return inference_session


def _create_session_token(
    *, session: str, account: str, expires: Optional[datetime]
) -> str:
    if expires is None:
        expires = timestamp.now() + timedelta(hours=1)
    grant = AccessGrant(
        resources=[Resources.InferenceSession],
        functions=[APIFunctions.get, APIFunctions.consume, APIFunctions.terminate],
        entities=[session],
    )
    session_key = tokens.generate_api_key(
        subject_type=Entities.Account,
        subject_id=account,
        grants=[grant],
        expires=expires,
    )
    return get_api_key_signer().sign_api_key(session_key)


def _session_internal_endpoint(session_id: str, session_endpoint: str) -> str:
    # If you're making requests to a session through the API, it is always
    # an independent session (i.e., not owned by an Evaluation)
    # FIXME: We need to read this format from the environment somehow, both
    # here, and in evaluation-client, and in dyff-operator, else it can get out of sync.
    k8s_service_name = f"i{session_id}i"
    return f"http://{k8s_service_name}.{config.kubernetes.workflows_namespace}:80/{session_endpoint}"


@app.post(
    f"/{Resources.InferenceSession}",
    tags=[Resources.InferenceSession],
    summary="Create an InferenceSession.",
    response_description="The created InferenceSession entity.",
)
def inferencesessions_create(
    inference_session_request: InferenceSessionCreateRequest,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> InferenceSessionAndToken:
    # inferencesessions.create for request account
    check_endpoint_permissions(
        api_key, endpoint, account=inference_session_request.account, id=None
    )
    # Referenced InferenceService
    service_id = inference_session_request.inferenceService
    service = query_backend.get_inference_service(service_id)
    service = require_entity_available(Entities.InferenceService, service_id, service)
    # inferenceservices.consume for referenced service
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.InferenceService.value,
            function=APIFunctions.consume.value,
        ),
        service,
    )

    session_dict = inference_session_request.dict()
    _set_system_fields(session_dict)
    session_dict["inferenceService"] = upcast(ForeignInferenceService, service).dict()
    inference_session = InferenceSession.parse_obj(session_dict)

    # FIXME: (DYFF-570) Reject .replicas > 1 for ObjectStorage until we
    # implement this feature.
    if (
        inference_session.inferenceService.model
        and inference_session.inferenceService.model.storage.medium
        == ModelStorageMedium.ObjectStorage
        and inference_session.replicas > 1
    ):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            detail=f"(DYFF-570): Models with storage.medium == ObjectStorage"
            " currently do not support .replicas > 1; try again with .replicas = 1",
        )

    command_backend.create_entity(inference_session)

    try:
        session_token = _create_session_token(
            session=inference_session.id,
            account=inference_session.account,
            expires=inference_session.expires,
        )
        return InferenceSessionAndToken(
            inferencesession=inference_session, token=session_token
        )
    except Exception as ex:
        command_backend.terminate_workflow(EntityIdentifier.of(inference_session))
        raise fastapi.HTTPException(
            fastapi.status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="failed to generate session token",
        ) from ex


@app.put(
    f"/{Resources.InferenceSession}/{{id}}/delete",
    tags=[Resources.InferenceSession],
    summary="Mark an InferenceSession for deletion.",
    response_description="The resulting status of the entity.",
)
def inferencesessions_delete(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    session = query_backend.get_inference_session(id)
    if session is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, session)
    if session.status != EntityStatus.deleted:
        command_backend.delete_entity(EntityIdentifier.of(session))
        return Status(
            status=EntityStatus.deleted, reason=EntityStatusReason.delete_command
        )
    else:
        return Status(status=session.status, reason=session.reason)


@app.put(
    f"/{Resources.InferenceSession}/{{id}}/terminate",
    tags=[Resources.InferenceSession],
    summary="Terminate an InferenceSession.",
)
def inferencesessions_terminate(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    session = query_backend.get_inference_session(id)
    if session is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, session)
    if session.status != EntityStatus.terminated:
        command_backend.terminate_workflow(EntityIdentifier.of(session))
        return Status(
            status=EntityStatus.terminated, reason=EntityStatusReason.terminate_command
        )
    else:
        return Status(status=session.status, reason=session.reason)


@app.post(
    f"/{Resources.InferenceSession}/{{id}}/infer/{{inference_endpoint:path}}",
    tags=[Resources.InferenceSession],
    summary="Create an InferenceSession.",
    response_description="The created InferenceSession entity.",
)
async def inferencesessions_infer(
    id: str,
    inference_endpoint: str,
    request: fastapi.Request,  # TODO: Define schemas for inference inputs
    inference_client: httpx.AsyncClient = fastapi.Depends(get_inference_async_client),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> fastapi.responses.StreamingResponse:
    # Note: Passing None for account means that the API key must have access
    # to this resource by ID. API keys associated with accounts won't work here.
    check_endpoint_permissions(
        api_key,
        Endpoint(resource=Resources.InferenceSession, function=APIFunctions.consume),
        id=id,
        account=None,
    )
    # FIXME: Define this URL in a canonical location
    # TODO: Should we expose the sessions at opaque IPs so that we don't need
    # to know that we're running in Kubernetes?
    internal_endpoint = _session_internal_endpoint(id, inference_endpoint)
    url = httpx.URL(internal_endpoint, query=request.url.query.encode("utf-8"))
    proxy_request = inference_client.build_request(
        request.method, url, headers=request.headers.raw, content=await request.body()
    )
    try:
        proxy_response = await inference_client.send(proxy_request, stream=True)
        return fastapi.responses.StreamingResponse(
            proxy_response.aiter_raw(),
            status_code=proxy_response.status_code,
            headers=proxy_response.headers,
            background=starlette.background.BackgroundTask(proxy_response.aclose),
        )
    except httpx.NetworkError:
        detail = (
            "Failed to connect to the session. Check that you have"
            " the correct session ID and the correct inference endpoint."
            " The session may not exist or may be unavailable. If the"
            f" session has .status == {EntityStatus.created}, wait for it to be"
            f" {EntityStatus.admitted}. If the session has .status == {EntityStatus.admitted},"
            " this may be a transient problem and you can retry after some time."
        )
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND, detail)
    except httpx.TimeoutException:
        detail = (
            "Connection to session timed out. The session may not be ready"
            f" to receive requests yet. If the session has .status == {EntityStatus.admitted},"
            " this is likely a transient problem and you can retry after some time."
        )
        raise fastapi.HTTPException(fastapi.status.HTTP_504_GATEWAY_TIMEOUT, detail)


def _ensure_timezone_utc(dt: datetime) -> datetime:
    if dt.tzinfo is None:
        # Naive datetimes are assumed to be UTC
        return dt.replace(tzinfo=timezone.utc)
    else:
        return dt.astimezone(timezone.utc)


@app.post(
    f"/{Resources.InferenceSession}/{{id}}/token",
    tags=[Resources.InferenceSession],
    summary="Get an access token for an existing InferenceSession.",
    response_description="A session access token.",
)
def inferencesessions_token(
    id: str,
    session_token_request: InferenceSessionTokenCreateRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
) -> str:
    session = query_backend.get_inference_session(id)
    if session is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    # inferencesessions.get
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.InferenceSession.value,
            function=APIFunctions.get.value,
        ),
        session,
    )
    # inferenceservices.consume for referenced service
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.InferenceService.value,
            function=APIFunctions.consume.value,
        ),
        session.inferenceService,
    )

    expires: Optional[datetime] = session_token_request.expires or session.expires
    if expires is not None:
        expires = _ensure_timezone_utc(expires)

    if (
        session.expires is not None
        and expires is not None
        and expires > session.expires
    ):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            detail="requested token expiration > session expiration",
        )

    try:
        return _create_session_token(
            session=session.id,
            account=session.account,
            expires=expires,
        )
    except Exception as ex:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="failed to generate session token",
        ) from ex


@app.get(
    f"/{Resources.InferenceSession}/{{id}}/ready",
    tags=[Resources.InferenceSession],
    summary="Perform a readiness probe on the session.",
    response_description="HTTP 200 if ready, 503 or other appropriate error if not ready.",
)
async def inferencesessions_ready(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    inference_client: httpx.AsyncClient = fastapi.Depends(get_inference_async_client),
    api_key: APIKey = fastapi.Depends(verified_api_key),
) -> fastapi.Response:
    """Check if an InferenceSession is ready. Returns status 200 if the session
    is ready. Raises a 503 (ServiceUnavailable) error if the session is not
    ready.

    Raises a 404 error if no session exists with the provided ID. Note
    that this may happen temporarily for session that were created
    recently, as it takes time for status information to propagate
    through the platform.
    """
    inference_session = query_backend.get_inference_session(id)
    if inference_session is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    # inferencesessions.get
    # We consider /ready to be an aspect of get
    # Note: We might want to not check permissions for /ready at all, since
    # it doesn't provide any sensitive information and we have to do a DB
    # query to perform the check.
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.InferenceSession,
            function=APIFunctions.get,
        ),
        inference_session,
    )

    internal_endpoint = _session_internal_endpoint(id, "ready")
    url = httpx.URL(internal_endpoint)
    # Short timeout because the pod could still be in a Pending status, so
    # instead of returning "not ready", the request will just time out.
    request = inference_client.build_request("GET", url, timeout=httpx.Timeout(0.5))
    try:
        response = await inference_client.send(request)
        response.raise_for_status()
    except httpx.HTTPError:
        return fastapi.Response(status_code=fastapi.status.HTTP_503_SERVICE_UNAVAILABLE)
    return fastapi.Response(status_code=fastapi.status.HTTP_200_OK)


# ----------------------------------------------------------------------------


@app.patch(
    f"/{Resources.Measurement}/{{id}}/labels",
    tags=[Resources.Measurement],
    summary="Update labels for an existing Measurement.",
)
def measurements_label(
    id: str,
    label_request: LabelsEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
):
    measurement = query_backend.get_measurement(id)
    if measurement is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, measurement)
    return command_backend.edit_entity_labels(
        EntityIdentifier.of(measurement), label_request
    )


@app.get(
    f"/{Resources.Measurement}",
    tags=[Resources.Measurement],
    summary="Get all Measurements matching a query.",
    response_description="The Measurements matching the query.",
)
def measurements_query(
    query: MeasurementQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[Measurement]:
    """Get all Measurements matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist = build_whitelist(api_key, endpoint)
    results = query_backend.query_measurements(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Measurement}/{{id}}",
    tags=[Resources.Measurement],
    summary="Get a Measurement by its key.",
    response_description="The Measurement with the given key.",
)
def measurements_get(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Measurement:
    """Get a Measurement by its key.

    Raises a 404 error if no entity exists with that key.
    """
    measurement = query_backend.get_measurement(id)
    if measurement is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, measurement)
    return measurement


@app.post(
    f"/{Resources.Measurement}",
    tags=[Resources.Measurement],
    summary="Create a Measurement.",
    response_description="The created Measurement entity.",
)
def measurements_create(
    analysis_request: AnalysisCreateRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Measurement:
    # Check permission to create analyses
    check_endpoint_permissions(
        api_key, endpoint, account=analysis_request.account, id=None
    )

    # Referenced Method
    method_id = analysis_request.method
    method = query_backend.get_method(method_id)
    method = require_entity_available(Entities.Method, method_id, method)
    # Check permission to consume referenced Method
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(resource=Resources.Method.value, function=APIFunctions.consume.value),
        method,
    )
    if method.output.kind != MethodOutputKind.Measurement:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            f"expected method.output.kind == Measurement; got {method.output.kind}",
        )
    if method.scope != MethodScope.Evaluation:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            f"expected method.scope == Evaluation; got {method.scope}",
        )
    if analysis_request.scope.evaluation is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            f"Method has Evaluation scope, but request.scope.evaluation is None",
        )

    validate_analysis_scope(query_backend, analysis_request.scope)

    input_kinds = {i.keyword: i.kind for i in method.inputs}
    for input_arg in analysis_request.inputs:
        input_id = input_arg.entity
        try:
            input_kind = input_kinds[input_arg.keyword]
        except KeyError:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_400_BAD_REQUEST,
                f"No method.parameter with keyword '{input_arg.keyword}'",
            )

        input_entity: Optional[DyffEntityType] = None
        if input_kind == MethodInputKind.Dataset:
            input_entity = query_backend.get_dataset(input_id)
        elif input_kind == MethodInputKind.Evaluation:
            input_entity = query_backend.get_evaluation(input_id)
        elif input_kind == MethodInputKind.Measurement:
            input_entity = query_backend.get_measurement(input_id)
        elif input_kind == MethodInputKind.Report:
            input_entity = query_backend.get_report(input_id)

        if input_entity is None:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_400_BAD_REQUEST,
                f"Unknown method.parameter kind {input_kind}",
            )

        # Check permission to consume referenced input
        check_endpoint_permissions_for_entity(
            api_key,
            Endpoint(
                resource=Resources.for_kind(Entities[input_entity.kind]).value,
                function=APIFunctions.consume.value,
            ),
            method,
        )

    # Check permission to consume referenced modules
    for module_id in method.modules:
        module = query_backend.get_module(module_id)
        if module is None:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_404_NOT_FOUND,
                f"module {module_id} not found",
            )
        check_endpoint_permissions_for_entity(
            api_key,
            Endpoint(
                resource=Resources.Module.value,
                function=APIFunctions.consume.value,
            ),
            module,
        )

    # Create an entity representing the output of the Analysis
    measurement_spec = method.output.measurement
    if measurement_spec is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Method spec violates constraints",
        )

    measurement = Measurement(
        account=analysis_request.account,
        id=ids.generate_entity_id(),
        creationTime=_initial_creationTime(),
        status=_initial_status(),
        scope=analysis_request.scope,
        method=upcast(ForeignMethod, method),
        arguments=analysis_request.arguments,
        inputs=analysis_request.inputs,
        # This covers: .name, .description, .level, .schema
        **measurement_spec.dict(),
    )
    command_backend.create_entity(measurement)
    return measurement


@app.put(
    f"/{Resources.Measurement}/{{id}}/delete",
    tags=[Resources.Measurement],
    summary="Mark a Measurement for deletion.",
    response_description="The resulting status of the entity.",
)
def measurements_delete(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    measurement = query_backend.get_measurement(id)
    if measurement is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, measurement)
    if measurement.status != EntityStatus.deleted:
        command_backend.delete_entity(EntityIdentifier.of(measurement))
        return Status(
            status=EntityStatus.deleted, reason=EntityStatusReason.delete_command
        )
    else:
        return Status(status=measurement.status, reason=measurement.reason)


@app.get(
    f"/{Resources.Measurement}/{{id}}/downlinks",
    tags=[Resources.Measurement],
    summary="Get a list of signed GET URLs from which measurement artifacts can be downloaded.",
    response_description="A list of signed GET URLs.",
)
def measurements_downlinks(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[ArtifactURL]:
    measurement = query_backend.get_measurement(id)
    if measurement is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no measurement {id}"
        )

    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.Measurement.value,
            function=APIFunctions.data.value,
        ),
        measurement,
    )

    if measurement.status != EntityStatus.complete:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected measurement.status == {EntityStatus.complete}; got {measurement.status}",
        )

    return list(storage.artifact_downlinks(Entities.Measurement, id))


@app.get(
    f"/{Resources.Measurement}/{{id}}/logs",
    tags=[Resources.Measurement],
    summary="Get a signed GET URL from which the logs file for the measurement run can be downloaded.",
    response_description="A signed GET URL.",
)
def measurements_logs(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Optional[ArtifactURL]:
    measurement = query_backend.get_measurement(id)
    if measurement is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no measurement {id}"
        )

    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.Measurement.value,
            function=APIFunctions.data.value,
        ),
        measurement,
    )

    if not is_status_terminal(measurement.status):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected measurement.status terminal; got {measurement.status}",
        )

    return storage.logs_downlink(Entities.Measurement, id)


# ----------------------------------------------------------------------------


@app.patch(
    f"/{Resources.Method}/{{id}}/labels",
    tags=[Resources.Method],
    summary="Update labels for an existing Method.",
)
def methods_label(
    id: str,
    label_request: LabelsEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
):
    method = query_backend.get_method(id)
    if method is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, method)
    return command_backend.edit_entity_labels(
        EntityIdentifier.of(method), label_request
    )


@app.get(
    f"/{Resources.Method}",
    tags=[Resources.Method],
    summary="Get all Methods matching a query.",
    response_description="The Methods matching the query.",
)
def methods_query(
    query: MethodQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[Method]:
    """Get all Methods matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist = build_whitelist(api_key, endpoint)
    results = query_backend.query_methods(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Method}/{{id}}",
    tags=[Resources.Method],
    summary="Get a Method by its key.",
    response_description="The Method with the given key.",
)
def methods_get(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Method:
    """Get a Method by its key.

    Raises a 404 error if no entity exists with that key.
    """
    method = query_backend.get_method(id)
    if method is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, method)
    return method


@app.post(
    f"/{Resources.Method}",
    tags=[Resources.Method],
    summary="Create a Method.",
    response_description="The created Method entity.",
)
def methods_create(
    method_request: MethodCreateRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Method:
    # Validation
    if method_request.output.kind == MethodOutputKind.Measurement:
        if method_request.output.measurement is None:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_400_BAD_REQUEST,
                "output.kind is Measurement but output.measurement not specified",
            )
    elif method_request.output.kind == MethodOutputKind.SafetyCase:
        if method_request.output.safetyCase is None:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_400_BAD_REQUEST,
                "output.kind is SafetyCase but output.safetyCase not specified",
            )
    else:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            f"Unknown output.kind {method_request.output.kind}",
        )

    # Check permission to create methods
    check_endpoint_permissions(
        api_key, endpoint, account=method_request.account, id=None
    )

    # Check permission to consume referenced modules
    for module_id in method_request.modules:
        module = query_backend.get_module(module_id)
        if module is None:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_404_NOT_FOUND,
                f"module {module_id} not found",
            )
        check_endpoint_permissions_for_entity(
            api_key,
            Endpoint(
                resource=Resources.Module.value,
                function=APIFunctions.consume.value,
            ),
            module,
        )

    method_dict = method_request.dict()
    _set_system_fields(method_dict)
    method = Method.parse_obj(method_dict)
    command_backend.create_entity(method)
    return method


@app.put(
    f"/{Resources.Method}/{{id}}/delete",
    tags=[Resources.Method],
    summary="Mark a Method for deletion.",
    response_description="The resulting status of the entity.",
)
def methods_delete(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    method = query_backend.get_method(id)
    if method is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, method)
    if method.status != EntityStatus.deleted:
        command_backend.delete_entity(EntityIdentifier.of(method))
        return Status(
            status=EntityStatus.deleted, reason=EntityStatusReason.delete_command
        )
    else:
        return Status(status=method.status, reason=method.reason)


@app.get(
    f"/{Resources.Method}/{{id}}/documentation",
    tags=[Resources.Method],
    summary="Get the documentation associated with a Method",
    response_description="The documentation for the Method",
)
def methods_documentation(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Documentation:
    """Get the documentation associated with a Method.

    Raises a 404 error if no entity exists with that key.
    """
    method = query_backend.get_method(id)
    if method is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(resource=Resources.Method.value, function=APIFunctions.get.value),
        method,
    )

    documentation = query_backend.get_documentation(id)
    return documentation or Documentation()


@app.patch(
    f"/{Resources.Method}/{{id}}/documentation",
    tags=[Resources.Method],
    summary="Edit the documentation associated with a Method",
)
def methods_edit_documentation(
    id: str,
    edit_request: DocumentationEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Documentation:
    """Edit the documentation associated with a Method.

    Raises a 404 error if no entity exists with that key. Returns the
    modified Documentation.
    """
    method = query_backend.get_method(id)
    if method is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(resource=Resources.Method.value, function=APIFunctions.edit.value),
        method,
    )

    documentation = query_backend.edit_documentation(id, edit_request)
    return documentation or Documentation()


# ----------------------------------------------------------------------------


@app.patch(
    f"/{Resources.Model}/{{id}}/labels",
    tags=[Resources.Model],
    summary="Update labels for an existing Model.",
)
def models_label(
    id: str,
    label_request: LabelsEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
):
    model = query_backend.get_model(id)
    if model is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, model)
    return command_backend.edit_entity_labels(EntityIdentifier.of(model), label_request)


@app.get(
    f"/{Resources.Model}",
    tags=[Resources.Model],
    summary="Get all Models matching a query.",
    response_description="The Models matching the query.",
)
def models_query(
    query: ModelQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[Model]:
    """Get all Models matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist = build_whitelist(api_key, endpoint)
    results = query_backend.query_models(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Model}/{{id}}",
    tags=[Resources.Model],
    summary="Get a Model by its key.",
    response_description="The Model with the given key.",
)
def models_get(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Model:
    """Get a Model by its key.

    Raises a 404 error if no entity exists with that key.
    """
    model = query_backend.get_model(id)
    if model is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, model)
    return model


@app.post(
    f"/{Resources.Model}",
    tags=[Resources.Model],
    summary="Create a Model.",
    response_description="The created Model entity.",
)
def models_create(
    model_request: ModelCreateRequest,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Model:
    # Check permission to create models
    check_endpoint_permissions(
        api_key, endpoint, account=model_request.account, id=None
    )

    model_dict = model_request.dict()
    _set_system_fields(model_dict)
    model = Model.parse_obj(model_dict)
    command_backend.create_entity(model)
    return model


@app.put(
    f"/{Resources.Model}/{{id}}/delete",
    tags=[Resources.Model],
    summary="Mark a Model for deletion.",
    response_description="The resulting status of the entity.",
)
def models_delete(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    model = query_backend.get_model(id)
    if model is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, model)
    if model.status != EntityStatus.deleted:
        command_backend.delete_entity(EntityIdentifier.of(model))
        return Status(
            status=EntityStatus.deleted, reason=EntityStatusReason.delete_command
        )
    else:
        return Status(status=model.status, reason=model.reason)


@app.get(
    f"/{Resources.Model}/{{id}}/documentation",
    tags=[Resources.Model],
    summary="Get the documentation associated with a Model",
    response_description="The documentation for the Model",
)
def models_documentation(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Documentation:
    """Get the documentation associated with a Model.

    Raises a 404 error if no entity exists with that key.
    """
    model = query_backend.get_model(id)
    if model is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(resource=Resources.Model.value, function=APIFunctions.get.value),
        model,
    )

    documentation = query_backend.get_documentation(id)
    return documentation or Documentation()


@app.patch(
    f"/{Resources.Model}/{{id}}/documentation",
    tags=[Resources.Model],
    summary="Edit the documentation associated with a Model",
)
def models_edit_documentation(
    id: str,
    edit_request: DocumentationEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Documentation:
    """Edit the documentation associated with a Model.

    Raises a 404 error if no entity exists with that key. Returns the
    modified Documentation.
    """
    model = query_backend.get_model(id)
    if model is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(resource=Resources.Model.value, function=APIFunctions.edit.value),
        model,
    )

    documentation = query_backend.edit_documentation(id, edit_request)
    return documentation or Documentation()


# ----------------------------------------------------------------------------


@app.patch(
    f"/{Resources.Module}/{{id}}/labels",
    tags=[Resources.Module],
    summary="Update labels for an existing Module.",
)
def modules_label(
    id: str,
    label_request: LabelsEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
):
    module = query_backend.get_module(id)
    if module is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, module)
    return command_backend.edit_entity_labels(
        EntityIdentifier.of(module), label_request
    )


@app.get(
    f"/{Resources.Module}",
    tags=[Resources.Module],
    summary="Get all Modules matching a query.",
    response_description="The Modules matching the query.",
)
def modules_query(
    query: ModuleQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[Module]:
    """Get all Modules matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist = build_whitelist(api_key, endpoint)
    results = query_backend.query_modules(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Module}/{{id}}",
    tags=[Resources.Module],
    summary="Get a Module by its key.",
    response_description="The Module with the given key.",
)
def modules_get(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Module:
    """Get a Module by its key.

    Raises a 404 error if no entity exists with that key.
    """
    module = query_backend.get_module(id)
    if module is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, module)
    return module


# Same design pattern as datasets_create
@app.post(
    f"/{Resources.Module}",
    tags=[Resources.Module],
    summary="Create a Module.",
    response_description="The created Module entity.",
)
def modules_create(
    module_request: ModuleCreateRequest,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Module:
    # Check permission to create modules
    check_endpoint_permissions(
        api_key, endpoint, account=module_request.account, id=None
    )
    # FIXME: This is (possibly?) gcloud-specific -- gcloud only provides md5 hashes
    if any(artifact.digest.md5 is None for artifact in module_request.artifacts):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            "gcloud storage requires artifact.digest.md5",
        )
    module_dict = module_request.dict()
    _set_system_fields(module_dict)
    module = Module.parse_obj(module_dict)
    # This .reason indicates that the orchestrator shouldn't attempt to
    # schedule the workflow because it's waiting for external input.
    module.reason = "WaitingForUpload"
    command_backend.create_entity(module)
    return module


@app.put(
    f"/{Resources.Module}/{{id}}/delete",
    tags=[Resources.Module],
    summary="Mark a Module for deletion.",
    response_description="The resulting status of the entity.",
)
def modules_delete(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    module = query_backend.get_module(id)
    if module is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, module)
    if module.status != EntityStatus.deleted:
        command_backend.delete_entity(EntityIdentifier.of(module))
        return Status(
            status=EntityStatus.deleted, reason=EntityStatusReason.delete_command
        )
    else:
        return Status(status=module.status, reason=module.reason)


@app.get(
    f"/{Resources.Module}/{{id}}/upload/{{artifact_path:path}}",
    tags=[Resources.Module],
    summary="Get a signed URL to which the given artifact can be uploaded.",
    response_description="A signed upload URL.",
)
def modules_upload(
    id: str,
    artifact_path: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> StorageSignedURL:
    module = query_backend.get_module(id)
    if module is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no module {id}"
        )
    check_endpoint_permissions_for_entity(api_key, endpoint, module)
    if module.status != EntityStatus.created:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected module.status == {EntityStatus.created}; got {module.status}",
        )
    for artifact in module.artifacts:
        if artifact.path == artifact_path:
            break
    else:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT, f"no artifact {artifact} in module"
        )
    return storage.signed_url_for_artifact_upload(
        artifact, storage.paths.module_root(id)
    )


@app.post(
    f"/{Resources.Module}/{{id}}/finalize",
    tags=[Resources.Module],
    summary="Indicate that all module artifacts have been uploaded.",
)
def modules_finalize(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> None:
    module = query_backend.get_module(id)
    if module is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no module {id}"
        )
    check_endpoint_permissions_for_entity(api_key, endpoint, module)

    if module.status == EntityStatus.ready:
        return
    elif module.status != EntityStatus.created:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected module.status == {EntityStatus.created}; got {module.status}",
        )

    for artifact in module.artifacts:
        if artifact.digest.md5 is None:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_500_INTERNAL_SERVER_ERROR,
                f"artifact.digest.md5 is None; this should have been caught at module creation",
            )

        try:
            storage_md5 = storage.artifact_md5hash(
                artifact, storage.paths.module_root(id)
            )
        except KeyError:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_409_CONFLICT,
                f"artifact not uploaded: {artifact.path}",
            )

        if storage_md5 != base64.b64decode(artifact.digest.md5):
            raise fastapi.HTTPException(
                fastapi.status.HTTP_409_CONFLICT, f"artifact hash {artifact.path}"
            )

    artifacts_dict = module.dict()["artifacts"]
    artifacts_json = json.dumps(artifacts_dict)
    storage.put_object(
        io.BytesIO(artifacts_json.encode()),
        f"{storage.paths.module_root(module.id)}/.dyff/artifacts.json",
    )
    command_backend.update_status(
        EntityIdentifier.of(module), status=EntityStatus.ready
    )


@app.get(
    f"/{Resources.Module}/{{id}}/downlinks",
    tags=[Resources.Module],
    summary="Get a list of signed GET URLs from which Module artifacts can be downloaded.",
    response_description="A list of signed GET URLs.",
)
def modules_downlinks(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[ArtifactURL]:
    module = query_backend.get_module(id)
    if module is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no module {id}"
        )

    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.Module.value,
            function=APIFunctions.data.value,
        ),
        module,
    )

    if module.status != EntityStatus.ready:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected module.status == {EntityStatus.ready}; got {module.status}",
        )

    return list(storage.artifact_downlinks(Entities.Module, id))


@app.get(
    f"/{Resources.Module}/{{id}}/documentation",
    tags=[Resources.Module],
    summary="Get the documentation associated with a Module",
    response_description="The documentation for the Module",
)
def modules_documentation(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Documentation:
    """Get the documentation associated with a Module.

    Raises a 404 error if no entity exists with that key.
    """
    module = query_backend.get_module(id)
    if module is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(resource=Resources.Module.value, function=APIFunctions.get.value),
        module,
    )

    documentation = query_backend.get_documentation(id)
    return documentation or Documentation()


@app.patch(
    f"/{Resources.Module}/{{id}}/documentation",
    tags=[Resources.Module],
    summary="Edit the documentation associated with a Module",
)
def modules_edit_documentation(
    id: str,
    edit_request: DocumentationEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Documentation:
    """Edit the documentation associated with a Module.

    Raises a 404 error if no entity exists with that key. Returns the
    modified Documentation.
    """
    module = query_backend.get_module(id)
    if module is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(resource=Resources.Module.value, function=APIFunctions.edit.value),
        module,
    )

    documentation = query_backend.edit_documentation(id, edit_request)
    return documentation or Documentation()


# ----------------------------------------------------------------------------


@app.get(
    f"/{Resources.UseCase}/{{id}}",
    tags=[Resources.UseCase],
    summary="Get a UseCase by its key.",
    response_description="The UseCase with the given key.",
)
def usecases_get(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> UseCase:
    """Get a UseCase by its key.

    Raises a 404 error if no entity exists with that key.
    """
    usecase = query_backend.get_usecase(id)
    if usecase is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, usecase)
    return usecase


@app.get(
    f"/{Resources.UseCase}",
    tags=[Resources.UseCase],
    summary="Get all UseCases matching a query.",
    response_description="The UseCases matching the query.",
)
def usecases_query(
    query: UseCaseQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[UseCase]:
    """Get all UseCases matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist = build_whitelist(api_key, endpoint)
    results = query_backend.query_usecases(whitelist, query)
    return list(results)


@app.post(
    f"/{Resources.UseCase}",
    tags=[Resources.UseCase],
    summary="Create a UseCase.",
    response_description="The created UseCase entity.",
)
def usecases_create(
    request: ConcernCreateRequest,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> UseCase:
    # Check permission to create modules
    check_endpoint_permissions(api_key, endpoint, account=request.account, id=None)
    usecase_dict = request.dict()
    _set_system_fields(usecase_dict)
    usecase = UseCase.parse_obj(usecase_dict)
    command_backend.create_entity(usecase)
    return usecase


@app.patch(
    f"/{Resources.UseCase}/{{id}}/labels",
    tags=[Resources.UseCase],
    summary="Update labels for an existing UseCase.",
)
def usecases_label(
    id: str,
    label_request: LabelsEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
):
    usecase = query_backend.get_usecase(id)
    if usecase is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, usecase)
    return command_backend.edit_entity_labels(
        EntityIdentifier.of(usecase), label_request
    )


@app.patch(
    f"/{Resources.UseCase}/{{id}}/documentation",
    tags=[Resources.UseCase],
    summary="Edit the documentation associated with a UseCase",
)
def usecases_edit_documentation(
    id: str,
    edit_request: DocumentationEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> None:
    """Edit the documentation associated with a UseCase.

    Raises a 404 error if no entity exists with that key.
    """
    usecase = query_backend.get_usecase(id)
    if usecase is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, usecase)
    edit = upcast(commands.EditEntityDocumentationPatch, edit_request.documentation)
    command_backend.edit_entity_documentation(EntityIdentifier.of(usecase), edit)


@app.put(
    f"/{Resources.UseCase}/{{id}}/delete",
    tags=[Resources.UseCase],
    summary="Mark a UseCase for deletion.",
    response_description="The resulting status of the entity.",
)
def usecases_delete(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    usecase = query_backend.get_usecase(id)
    if usecase is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, usecase)
    if usecase.status != EntityStatus.deleted:
        command_backend.delete_entity(EntityIdentifier.of(usecase))
        return Status(
            status=EntityStatus.deleted, reason=EntityStatusReason.delete_command
        )
    else:
        return Status(status=usecase.status, reason=usecase.reason)


# ----------------------------------------------------------------------------


@app.patch(
    f"/{Resources.Report}/{{id}}/labels",
    tags=[Resources.Report],
    summary="Update labels for an existing Report.",
)
def reports_label(
    id: str,
    label_request: LabelsEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
):
    report = query_backend.get_report(id)
    if report is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, report)
    return command_backend.edit_entity_labels(
        EntityIdentifier.of(report), label_request
    )


@app.get(
    f"/{Resources.Report}",
    tags=[Resources.Report],
    summary="Get all Reports matching a query.",
    response_description="The Reports matching the query.",
)
def reports_query(
    query: ReportQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[Report]:
    """Get all Reports matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist = build_whitelist(api_key, endpoint)
    results = query_backend.query_reports(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.Report}/{{id}}",
    tags=[Resources.Report],
    summary="Get a Report by its key.",
    response_description="The Report with the given key.",
)
def reports_get(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Report:
    """Get a Report by its key.

    Raises a 404 error if no entity exists with that key.
    """
    report = query_backend.get_report(id)
    if report is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, report)
    return report


@app.post(
    f"/{Resources.Report}",
    tags=[Resources.Report],
    summary="Create a Report.",
    response_description="The created Report entity.",
)
def reports_create(
    report_request: ReportCreateRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Report:
    # Check permission to create reports
    check_endpoint_permissions(
        api_key, endpoint, account=report_request.account, id=None
    )

    # Referenced Evaluation
    evaluation_id = report_request.evaluation
    evaluation = query_backend.get_evaluation(evaluation_id)
    evaluation = require_entity_available(
        Entities.Evaluation, evaluation_id, evaluation
    )
    evaluation_view = None
    if isinstance(report_request.evaluationView, str):
        service = evaluation.inferenceSession.inferenceService
        for view in service.outputViews:
            if view.id == report_request.evaluationView:
                evaluation_view = view
                break
        else:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_404_NOT_FOUND,
                f"no View with .id {report_request.evaluationView}"
                f" for InferenceService {service.id}",
            )
    else:
        # Also covers the case where evaluationView is None
        evaluation_view = report_request.evaluationView
    # Check permission to consume referenced Evaluation
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.Evaluation.value, function=APIFunctions.consume.value
        ),
        evaluation,
    )

    # Referenced Dataset
    dataset_id = evaluation.dataset
    dataset = query_backend.get_dataset(dataset_id)
    dataset = require_entity_available(Entities.Dataset, dataset_id, dataset)
    dataset_view = None
    if isinstance(report_request.datasetView, str):
        for view in dataset.views:
            if view.id == report_request.datasetView:
                dataset_view = view
                break
        else:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_404_NOT_FOUND,
                f"no View with .id {report_request.datasetView} for Dataset {dataset.id}",
            )
    else:
        # Also covers the case where datasetView is None
        dataset_view = report_request.datasetView
    # Check permission to consume referenced InferenceService
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.Dataset.value,
            # TODO: There need to be separate roles for consuming input
            # instances vs. consuming labels and covariates, and Dataset needs
            # a way to annotate which category each field belongs to.
            function=APIFunctions.consume.value,
        ),
        dataset,
    )

    # Check permission to consume referenced modules
    for module_id in report_request.modules:
        module = query_backend.get_module(module_id)
        if module is None:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_404_NOT_FOUND,
                f"module {module_id} not found",
            )
        check_endpoint_permissions_for_entity(
            api_key,
            Endpoint(
                resource=Resources.Module.value,
                function=APIFunctions.consume.value,
            ),
            module,
        )

    report_dict = report_request.dict()
    _set_system_fields(report_dict)
    service = evaluation.inferenceSession.inferenceService
    report_dict["dataset"] = dataset.id
    report_dict["inferenceService"] = service.id
    report_dict["model"] = service.model.id if service.model else None
    report_dict["datasetView"] = dataset_view and dataset_view.dict()
    report_dict["evaluationView"] = evaluation_view and evaluation_view.dict()
    report = Report.parse_obj(report_dict)
    command_backend.create_entity(report)
    return report


@app.put(
    f"/{Resources.Report}/{{id}}/delete",
    tags=[Resources.Report],
    summary="Mark a Report for deletion.",
    response_description="The resulting status of the entity.",
)
def reports_delete(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    report = query_backend.get_report(id)
    if report is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, report)
    if report.status != EntityStatus.deleted:
        command_backend.delete_entity(EntityIdentifier.of(report))
        return Status(
            status=EntityStatus.deleted, reason=EntityStatusReason.delete_command
        )
    else:
        return Status(status=report.status, reason=report.reason)


@app.get(
    f"/{Resources.Report}/{{id}}/downlinks",
    tags=[Resources.Report],
    summary="Get a list of signed GET URLs from which Report artifacts can be downloaded.",
    response_description="A list of signed GET URLs.",
)
def reports_downlinks(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[ArtifactURL]:
    report = query_backend.get_report(id)
    if report is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no report {id}"
        )

    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.Report.value,
            function=APIFunctions.data.value,
        ),
        report,
    )

    if report.status != EntityStatus.complete:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected report.status == {EntityStatus.complete}; got {report.status}",
        )

    return list(storage.artifact_downlinks(Entities.Report, id))


@app.get(
    f"/{Resources.Report}/{{id}}/logs",
    tags=[Resources.Report],
    summary="Get a signed GET URL from which the logs file for the report run can be downloaded.",
    response_description="A signed GET URL.",
)
def reports_logs(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Optional[ArtifactURL]:
    report = query_backend.get_report(id)
    if report is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no report {id}"
        )

    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.Report.value,
            function=APIFunctions.data.value,
        ),
        report,
    )

    if not is_status_terminal(report.status):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected report.status terminal; got {report.status}",
        )

    return storage.logs_downlink(Entities.Report, id)


# ----------------------------------------------------------------------------


# @app.get(
#     f"/{Resources.Revision}/{{revision_id}}",
#     tags=[Resources.Revision],
#     summary="Get a Revision by its key.",
#     response_description="The Revision with the given key.",
# )
# def revisions_get(
#     revision_id: str,
#     query_backend: QueryBackend = fastapi.Depends(get_query_backend),
#     api_key: APIKey = fastapi.Depends(verified_api_key),
#     endpoint: Endpoint = fastapi.Depends(get_endpoint),
# ) -> Revision:
#     """Get a Revision by its key.

#     Raises a 404 error if no entity exists with that key.
#     """
#     raise NotImplementedError()
#     # revision = query_backend.get_revision(revision_id)
#     # if revision is None:
#     #     raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
#     # check_endpoint_permissions_for_entity(api_key, endpoint, revision)
#     # return revision


# ----------------------------------------------------------------------------


@app.patch(
    f"/{Resources.SafetyCase}/{{id}}/labels",
    tags=[Resources.SafetyCase],
    summary="Update labels for an existing SafetyCase.",
)
def safetycases_label(
    id: str,
    label_request: LabelsEditRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
):
    safetycase = query_backend.get_safetycase(id)
    if safetycase is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, safetycase)
    return command_backend.edit_entity_labels(
        EntityIdentifier.of(safetycase), label_request
    )


@app.get(
    f"/{Resources.SafetyCase}",
    tags=[Resources.SafetyCase],
    summary="Get all SafetyCase entities matching a query.",
    response_description="The SafetyCase entities matching the query.",
)
def safetycases_query(
    query: SafetyCaseQueryRequest = fastapi.Depends(),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[SafetyCase]:
    """Get all SafetyCase entities matching a query.

    The query is a set of equality constraints specified as key-value
    pairs.
    """
    whitelist = build_whitelist(api_key, endpoint)
    results = query_backend.query_safetycases(whitelist, query)
    return list(results)


@app.get(
    f"/{Resources.SafetyCase}/{{id}}",
    tags=[Resources.SafetyCase],
    summary="Get a SafetyCase by its key.",
    response_description="The SafetyCase with the given key.",
)
def safetycases_get(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> SafetyCase:
    """Get a SafetyCase by its key.

    Raises a 404 error if no entity exists with that key.
    """
    safetycase = query_backend.get_safetycase(id)
    if safetycase is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, safetycase)
    return safetycase


@app.post(
    f"/{Resources.SafetyCase}",
    tags=[Resources.SafetyCase],
    summary="Create a SafetyCase.",
    response_description="The created SafetyCase entity.",
)
def safetycases_create(
    analysis_request: AnalysisCreateRequest,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> SafetyCase:
    # Check permission to create analyses
    check_endpoint_permissions(
        api_key, endpoint, account=analysis_request.account, id=None
    )

    # Referenced Method
    method_id = analysis_request.method
    method = query_backend.get_method(method_id)
    method = require_entity_available(Entities.Method, method_id, method)
    # Check permission to consume referenced Method
    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(resource=Resources.Method.value, function=APIFunctions.consume.value),
        method,
    )
    if method.output.kind != MethodOutputKind.SafetyCase:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            f"expected method.output.kind == SafetyCase; got {method.output.kind}",
        )
    if method.scope != MethodScope.InferenceService:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            f"expected method.scope == InferenceService; got {method.scope}",
        )
    if analysis_request.scope.inferenceService is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            f"Method has InferenceService scope, but request.scope.inferenceService is None",
        )

    validate_analysis_scope(query_backend, analysis_request.scope)

    input_kinds = {i.keyword: i.kind for i in method.inputs}
    for input_arg in analysis_request.inputs:
        input_id = input_arg.entity
        try:
            input_kind = input_kinds[input_arg.keyword]
        except KeyError:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_400_BAD_REQUEST,
                f"No method.parameter with keyword '{input_arg.keyword}'",
            )

        input_entity: Optional[DyffEntityType] = None
        if input_kind == MethodInputKind.Dataset:
            input_entity = query_backend.get_dataset(input_id)
        elif input_kind == MethodInputKind.Evaluation:
            input_entity = query_backend.get_evaluation(input_id)
        elif input_kind == MethodInputKind.Measurement:
            input_entity = query_backend.get_measurement(input_id)
        elif input_kind == MethodInputKind.Report:
            input_entity = query_backend.get_report(input_id)
        else:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_400_BAD_REQUEST,
                f"Unknown method.parameter kind {input_kind}",
            )

        if input_entity is None:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_404_NOT_FOUND,
                f"input entity '{input_kind}/{input_id}' not found",
            )

        # Check permission to consume referenced input
        check_endpoint_permissions_for_entity(
            api_key,
            Endpoint(
                resource=Resources.for_kind(Entities[input_entity.kind]).value,
                function=APIFunctions.consume.value,
            ),
            method,
        )

    # Check permission to consume referenced modules
    for module_id in method.modules:
        module = query_backend.get_module(module_id)
        if module is None:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_404_NOT_FOUND,
                f"module {module_id} not found",
            )
        check_endpoint_permissions_for_entity(
            api_key,
            Endpoint(
                resource=Resources.Module.value,
                function=APIFunctions.consume.value,
            ),
            module,
        )

    # Create an entity representing the output of the Analysis
    safetycase_spec = method.output.safetyCase
    if safetycase_spec is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Method spec violates constraints",
        )

    # Populate additional context data
    def get_system() -> Union[InferenceService, Model]:
        if analysis_request.scope.inferenceService is None:
            raise ValueError("Must specify at least request.scope.inferenceService")
        service = query_backend.get_inference_service(
            analysis_request.scope.inferenceService
        )
        if service is None:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_404_NOT_FOUND,
                f"inferenceservices/{analysis_request.scope.inferenceService}",
            )
        if service.model is not None:
            model = query_backend.get_model(service.model.id)
            if model is None:
                raise fastapi.HTTPException(
                    fastapi.status.HTTP_404_NOT_FOUND,
                    f"models/{service.model.id}",
                )
            return model
        else:
            return service

    system = get_system()

    def title_fallback(thing: Union[InferenceService, Method, Model]) -> str:
        if thing.name.strip() != "":
            return thing.name
        else:
            return _namespaced_id(thing)

    system_documentation = query_backend.get_documentation(system.id)
    if system_documentation is None:
        system_documentation = Documentation(
            entity=system.id,
        )
    if system_documentation.title is None:
        system_documentation.title = title_fallback(system)
    if system_documentation.summary is None:
        system_documentation.summary = ""

    usecase_documentation = query_backend.get_documentation(method.id)
    if usecase_documentation is None:
        usecase_documentation = Documentation(
            entity=method.id,
        )
    if usecase_documentation.title is None:
        usecase_documentation.title = title_fallback(method)
    if usecase_documentation.summary is None:
        usecase_documentation.summary = ""

    class SystemData(DyffSchemaBaseModel):
        spec: Union[Model, InferenceService]
        documentation: Documentation

    class UseCaseData(DyffSchemaBaseModel):
        spec: Method
        documentation: Documentation

    system_data = SystemData(spec=system, documentation=system_documentation)
    usecase_data = UseCaseData(spec=method, documentation=usecase_documentation)

    def encode(data: DyffSchemaBaseModel) -> str:
        return base64.b64encode(data.json().encode()).decode()

    safetycase = SafetyCase(
        account=analysis_request.account,
        id=ids.generate_entity_id(),
        creationTime=_initial_creationTime(),
        status=_initial_status(),
        scope=analysis_request.scope,
        method=upcast(ForeignMethod, method),
        arguments=analysis_request.arguments,
        inputs=analysis_request.inputs,
        data=[
            AnalysisData(
                key="system",
                value=encode(system_data),
            ),
            AnalysisData(
                key="usecase",
                value=encode(usecase_data),
            ),
        ],
        # This covers: .name, .description
        **safetycase_spec.dict(),
    )
    command_backend.create_entity(safetycase)
    return safetycase


@app.put(
    f"/{Resources.SafetyCase}/{{id}}/delete",
    tags=[Resources.SafetyCase],
    summary="Mark a SafetyCase for deletion.",
    response_description="The resulting status of the entity.",
)
def safetycases_delete(
    id: str,
    command_backend: CommandBackend = fastapi.Depends(get_command_backend),
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Status:
    safetycase = query_backend.get_safetycase(id)
    if safetycase is None:
        raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    check_endpoint_permissions_for_entity(api_key, endpoint, safetycase)
    if safetycase.status != EntityStatus.deleted:
        command_backend.delete_entity(EntityIdentifier.of(safetycase))
        return Status(
            status=EntityStatus.deleted, reason=EntityStatusReason.delete_command
        )
    else:
        return Status(status=safetycase.status, reason=safetycase.reason)


@app.get(
    f"/{Resources.SafetyCase}/{{id}}/downlinks",
    tags=[Resources.SafetyCase],
    summary="Get a list of signed GET URLs from which safety case artifacts can be downloaded.",
    response_description="A list of signed GET URLs.",
)
def safetycases_downlinks(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> list[ArtifactURL]:
    safetycase = query_backend.get_safetycase(id)
    if safetycase is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no safetycase {id}"
        )

    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.SafetyCase.value,
            function=APIFunctions.data.value,
        ),
        safetycase,
    )

    if safetycase.status != EntityStatus.complete:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected safetycase.status == {EntityStatus.complete}; got {safetycase.status}",
        )

    return list(storage.artifact_downlinks(Entities.SafetyCase, id))


@app.get(
    f"/{Resources.SafetyCase}/{{id}}/logs",
    tags=[Resources.SafetyCase],
    summary="Get a signed GET URL from which the logs file for the safety case run can be downloaded.",
    response_description="A signed GET URL.",
)
def safetycases_logs(
    id: str,
    query_backend: QueryBackend = fastapi.Depends(get_query_backend),
    api_key: APIKey = fastapi.Depends(verified_api_key),
    endpoint: Endpoint = fastapi.Depends(get_endpoint),
) -> Optional[ArtifactURL]:
    safetycase = query_backend.get_safetycase(id)
    if safetycase is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, f"no safetycase {id}"
        )

    check_endpoint_permissions_for_entity(
        api_key,
        Endpoint(
            resource=Resources.SafetyCase.value,
            function=APIFunctions.data.value,
        ),
        safetycase,
    )

    if not is_status_terminal(safetycase.status):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_409_CONFLICT,
            f"expected safetycase.status terminal; got {safetycase.status}",
        )

    return storage.logs_downlink(Entities.SafetyCase, id)
