# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import itertools
from datetime import datetime, timedelta, timezone
from typing import Iterable

import click

from dyff.schema.platform import (
    Dataset,
    Evaluation,
    InferenceService,
    InferenceSession,
    Measurement,
    Method,
    Model,
    Module,
    Report,
    SafetyCase,
)
from dyff.storage.backend.base.command import CommandBackend
from dyff.storage.backend.base.query import QueryBackend

from .. import dynamic_import
from ..config import config


@click.group()
@click.pass_context
def entities(ctx: click.Context):
    ctx.ensure_object(dict)
    command_backend: CommandBackend = dynamic_import.instantiate(
        config.api.command.backend
    )
    query_backend: QueryBackend = dynamic_import.instantiate(config.api.query.backend)
    ctx.obj["command_backend"] = command_backend
    ctx.obj["query_backend"] = query_backend
    ctx.call_on_close(command_backend.close)


def _expired(ctx: click.Context, before: datetime) -> Iterable[str]:
    query_backend: QueryBackend = ctx.obj["query_backend"]
    for e in itertools.chain(
        query_backend.expired_entities(Dataset, before=before),
        query_backend.expired_entities(Evaluation, before=before),
        query_backend.expired_entities(InferenceService, before=before),
        query_backend.expired_entities(InferenceSession, before=before),
        query_backend.expired_entities(Measurement, before=before),
        query_backend.expired_entities(Method, before=before),
        query_backend.expired_entities(Model, before=before),
        query_backend.expired_entities(Module, before=before),
        query_backend.expired_entities(Report, before=before),
        query_backend.expired_entities(SafetyCase, before=before),
    ):
        yield e.id


@entities.command()
@click.pass_context
@click.argument(
    "operation",
    type=click.Choice(["list", "forget"]),
    metavar="OPERATION",
    required=False,
    default="list",
)
@click.option(
    "days",
    "--days",
    metavar="DAYS",
    type=float,
    default=0,
    help="Forget *all entities* older than this many days.",
)
@click.option(
    "hours",
    "--hours",
    metavar="HOURS",
    type=float,
    default=0,
    help="Forget *all entities* older than this many hours.",
)
@click.option(
    "minutes",
    "--minutes",
    metavar="MINUTES",
    type=float,
    default=0,
    help="Forget *all entities* older than this many minutes.",
)
@click.option(
    "seconds",
    "--seconds",
    metavar="SECONDS",
    type=float,
    default=0,
    help="Forget *all entities* older than this many seconds.",
)
@click.option(
    "yes",
    "--yes",
    metavar="YES",
    is_flag=True,
    default=False,
    help="If performing a mutating operation, you must confirm it with '--yes',"
    " else no mutation will be performed.",
)
def expired(
    ctx: click.Context,
    operation: str,
    days: float,
    hours: float,
    minutes: float,
    seconds: float,
    yes: bool,
):
    try:
        command_backend: CommandBackend = ctx.obj["command_backend"]
        delta = timedelta(
            days=days,
            hours=hours,
            minutes=minutes,
            seconds=seconds,
        )
        before = datetime.now(timezone.utc) - delta
        expired_ids = list(_expired(ctx, before))
        for id in expired_ids:
            click.echo(id)
            if operation == "forget" and yes:
                command_backend.forget_entity(id)
        if operation == "forget" and not yes:
            click.echo(f"{len(expired_ids)} entities will be forgotten.")
            click.echo("Run again with '--yes' to confirm.")
    except Exception as ex:
        raise click.ClickException(" ".join(ex.args)) from ex
