# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from typing import Optional

import click

from dyff.schema.platform import Status
from dyff.storage.backend.base.command import CommandBackend

from .. import dynamic_import
from ..config import config


@click.group()
@click.pass_context
def commands(ctx: click.Context):
    ctx.ensure_object(dict)
    command_backend: CommandBackend = dynamic_import.instantiate(
        config.api.command.backend
    )
    ctx.obj["command_backend"] = command_backend
    ctx.call_on_close(command_backend.close)


@commands.command()
@click.pass_context
@click.argument("command", type=str, metavar="COMMAND", required=True)
@click.argument("entity", type=str, metavar="ENTITY", required=True)
@click.argument("body", type=str, metavar="BODY", required=False)
def exec(ctx: click.Context, command: str, entity: str, body: Optional[str]):
    try:
        command_backend: CommandBackend = ctx.obj["command_backend"]
        if command == "ForgetEntity":
            command_backend.forget_entity(entity)
        elif command == "UpdateEntityStatus":
            if body is None:
                raise ValueError(f"command {command} requires body")
            command_backend.update_status(
                entity, **Status.parse_raw(body).model_dump(mode="json")
            )
        else:
            raise NotImplementedError(command)
    except Exception as ex:
        raise click.ClickException(" ".join(ex.args)) from ex
