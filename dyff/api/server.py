# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0
"""Web server for the dyff platform."""

import logging
from contextlib import asynccontextmanager
from typing import Optional

import fastapi
from fastapi.responses import RedirectResponse

from . import api, auth, views
from ._version import __version__
from .config import config


class EndpointFilter(logging.Filter):
    def __init__(self, path: str, *args, verb: Optional[str] = None, **kwargs):
        super().__init__(*args, **kwargs)
        self._path = path
        self._verb = verb

    def filter(self, record):
        (
            remote_address,
            verb,
            query_string,
            html_version,
            status_code,
            *rest,
        ) = record.args
        return not all(
            [
                self._verb is None or verb == self._verb,
                query_string.startswith(self._path),
            ]
        )


try:
    import uvicorn.config

    # Filter out log messages about the '/health' endpoint, because they happen
    # a lot and they're not interesting.
    uvicorn_logger = logging.getLogger("uvicorn.access")
    uvicorn_logger.addFilter(EndpointFilter(path="/health"))
    uvicorn.config.LOGGING_CONFIG["loggers"]["api-server"] = {
        "handlers": ["default"],
        "level": logging.INFO,
    }
except Exception:
    pass


@asynccontextmanager
async def _lifespan(app: fastapi.FastAPI):
    # FIXME: Try as I might, I can't get logging to print anything unless I go
    # through the 'uvicorn' logger. Since all I really want to do is see the
    # configuration, I'm just going to use a print() for now.
    # log = logging.getLogger("api-server")
    # log.info(f"aStarting API server with configuration:\n{config.json(indent=2)}")
    # log = logging.getLogger("uvicorn")
    # log.info(f"bStarting API server with configuration:\n{config.json(indent=2)}")
    print(
        f"Starting API server with configuration:\n{config.json(indent=2)}", flush=True
    )
    yield


app = fastapi.FastAPI(
    title="Dyff API",
    version=__version__,
    lifespan=_lifespan,
    redoc_url=None,
)
app.mount("/v0", api.app)
# app.mount("/public/v0", public.app)

app.mount("/auth", auth.app)
app.mount("/views", views.app)


@app.get(f"/health")
async def health() -> int:
    return fastapi.status.HTTP_200_OK


@app.get("/")
async def redirect_root_to_docs():
    return RedirectResponse(url="/v0/docs", status_code=fastapi.status.HTTP_302_FOUND)
