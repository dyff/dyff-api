# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import functools
import itertools
import json
from typing import Collection, Iterable, Optional, TypeVar

import fastapi

from dyff.api import dynamic_import
from dyff.api.config import config
from dyff.schema.platform import (
    APIFunctions,
    APIKey,
    Documentation,
    DyffEntity,
    DyffSchemaBaseModel,
    Entities,
    InferenceService,
    Labeled,
    Measurement,
    Method,
    Model,
    Resources,
    SafetyCase,
    Score,
    UseCase,
)
from dyff.schema.requests import (
    DocumentationQueryRequest,
    InferenceServiceQueryRequest,
    MeasurementQueryRequest,
    MethodQueryRequest,
    ModelQueryRequest,
    SafetyCaseQueryRequest,
    ScoreQueryRequest,
    UseCaseQueryRequest,
)
from dyff.storage.backend.base.auth import AuthBackend
from dyff.storage.backend.base.query import QueryBackend, Whitelist

from ._version import __version__
from .api import (
    Endpoint,
    build_whitelist,
    check_endpoint_permissions_for_entity,
    get_auth_backend,
    get_security_scheme,
    verify_api_key,
)
from .public import QueryT, is_public_access, query_for_public_access


def is_preview_access(entity: DyffEntity) -> bool:
    access = entity.labels.get("dyff.io/access")
    return access == "internal"


def check_preview_access(entity: DyffEntity) -> None:
    if not is_preview_access(entity):
        raise fastapi.HTTPException(
            fastapi.status.HTTP_403_FORBIDDEN, f"entity {entity.id} is not public"
        )


def query_for_preview_access(query: QueryT) -> QueryT:
    """Return a modified query request that will query all entities that match
    the original query request and are also marked for preview (internal)
    access.

    The original query request is not modified.
    """
    # .labels is an Optional[str] containing a JSON document
    if query.labels is not None:
        labels_dict = json.loads(query.labels)
    else:
        labels_dict = {}
    labels_dict["dyff.io/access"] = "internal"
    return query.copy(update={"labels": json.dumps(labels_dict)})


class _ViewQueryBackend:
    """A wrapper for a QueryBackend that does permission checks before
    returning results.

    TODO: Currently, this only implements the methods required for /views
    routes. But we should complete the implementation and use it everywhere,
    because it's less error-prone to do the authentication all in one place.
    """

    def __init__(self, base: QueryBackend, api_key: Optional[APIKey] = None):
        self._base = base
        self._api_key = api_key

    def _authenticated_query(self, query_fn, kind: Entities, request: QueryT):
        public_whitelist, public_query = query_for_public_access(request)
        public_entities = query_fn(public_whitelist, public_query)

        if self._api_key:
            preview_whitelist = build_whitelist(
                self._api_key, Endpoint(Resources.for_kind(kind), APIFunctions.query)
            )
            preview_query = query_for_preview_access(request)
            preview_entities = query_fn(preview_whitelist, preview_query)
        else:
            preview_entities = []

        # Note: Currently, we know there are no duplicates because the possible
        # "access" labels are mutually exclusive. (Unless someone modifies the
        # label in between the two queries).
        return list(itertools.chain(public_entities, preview_entities))

    def _authenticated_get(self, get_fn, kind: Entities, id: str):
        entity = get_fn(id)
        if entity is None:
            return None
        if is_public_access(entity):
            return entity
        if self._api_key:
            check_endpoint_permissions_for_entity(
                self._api_key,
                Endpoint(Resources.for_kind(kind), APIFunctions.get),
                entity,
            )
            if is_preview_access(entity):
                return entity
        raise fastapi.HTTPException(
            fastapi.status.HTTP_401_UNAUTHORIZED, "resource is not public"
        )

    def unsafe_query_documentation(
        self, entity_ids: list[str]
    ) -> Collection[Documentation]:
        """Get the Documentation objects for the specified entity IDs.

        .. warning:: Does not check permissions!

            You must check permissions on the *corresponding entity* first to
            ensure that fetching documentation is allowed.
        """
        return self._base.query_documentation(
            Whitelist.everything(),
            DocumentationQueryRequest(query=json.dumps({"id": entity_ids})),
        )

    def get_inference_service(self, id: str) -> Optional[InferenceService]:
        return self._authenticated_get(
            self._base.get_inference_service, Entities.InferenceService, id
        )

    def query_inference_services(
        self, request: InferenceServiceQueryRequest
    ) -> Collection[InferenceService]:
        return self._authenticated_query(
            self._base.query_inference_services, Entities.InferenceService, request
        )

    def query_measurements(
        self, request: MeasurementQueryRequest
    ) -> Collection[Measurement]:
        return self._authenticated_query(
            self._base.query_measurements, Entities.Measurement, request
        )

    def get_method(self, id: str) -> Optional[Method]:
        return self._authenticated_get(self._base.get_method, Entities.Method, id)

    def query_methods(self, request: MethodQueryRequest) -> Collection[Method]:
        return self._authenticated_query(
            self._base.query_methods, Entities.Method, request
        )

    def get_model(self, id: str) -> Optional[Model]:
        return self._authenticated_get(self._base.get_model, Entities.Model, id)

    def query_models(self, request: ModelQueryRequest) -> Collection[Model]:
        return self._authenticated_query(
            self._base.query_models, Entities.Model, request
        )

    def query_safetycases(
        self, request: SafetyCaseQueryRequest
    ) -> Collection[SafetyCase]:
        return self._authenticated_query(
            self._base.query_safetycases, Entities.SafetyCase, request
        )

    def query_usecases(self, request: UseCaseQueryRequest) -> Collection[UseCase]:
        return self._authenticated_query(
            self._base.query_usecases, Entities.UseCase, request
        )

    # def get_score(self, id: str) -> Optional[Score]:
    #     return self._authenticated_get(self._base.get_score, Entities.Score, id)

    def unsafe_query_scores(
        self,
        analysis_ids: list[str],
    ) -> Collection[Score]:
        return self._base.query_scores(
            Whitelist.everything(),
            ScoreQueryRequest(query=json.dumps({"analysis": analysis_ids})),
        )


@functools.lru_cache()
def _get_query_backend_base() -> QueryBackend:
    return dynamic_import.instantiate(config.api.query.backend)


def get_query_backend(
    api_token: Optional[str] = fastapi.Depends(get_security_scheme(auto_error=False)),
    auth_backend: AuthBackend = fastapi.Depends(get_auth_backend),
) -> _ViewQueryBackend:
    if api_token:
        api_key = verify_api_key(api_token, auth_backend)
        return _ViewQueryBackend(_get_query_backend_base(), api_key)
    else:
        return _ViewQueryBackend(_get_query_backend_base())


def _generate_unique_id(route: fastapi.routing.APIRoute) -> str:
    """Strip everything but the function name from the ``operationId``
    fields."""
    return route.name


app = fastapi.FastAPI(
    title="Aggregate views for the Dyff Web app",
    generate_unique_id_function=_generate_unique_id,
    redoc_url=None,
    version=__version__,
)


# ----------------------------------------------------------------------------


class SystemSingleView(DyffSchemaBaseModel):
    model: Optional[Model]
    services: list[InferenceService]
    methods: list[Method]
    measurements: list[Measurement]
    safetycases: list[SafetyCase]
    documentation: list[Documentation]
    scores: list[Score]
    usecases: list[UseCase]


class SystemListView(DyffSchemaBaseModel):
    models: list[Model]
    services: list[InferenceService]
    documentation: list[Documentation]


class MethodSingleView(DyffSchemaBaseModel):
    method: Method
    measurements: list[Measurement]
    safetycases: list[SafetyCase]
    services: list[InferenceService]
    models: list[Model]
    documentation: list[Documentation]
    scores: list[Score]
    usecases: list[UseCase]


class MethodListView(DyffSchemaBaseModel):
    methods: list[Method]
    documentation: list[Documentation]
    usecases: list[UseCase]


class HomeView(DyffSchemaBaseModel):
    methods: list[Method]
    models: list[Model]
    usecases: list[UseCase]
    documentation: list[Documentation]


def _newest_analyses_grouped_by_system(
    analyses: Iterable[SafetyCase],
) -> dict[str, SafetyCase]:
    # Map of system_id (model or service) -> newest SafetyCase for that system
    newest_analyses: dict[str, SafetyCase] = {}
    for analysis in analyses:
        system = analysis.scope.model or analysis.scope.inferenceService
        if system is None:
            continue
        incumbent = newest_analyses.setdefault(system, analysis)
        if incumbent.creationTime < analysis.creationTime:
            newest_analyses[system] = analysis
    return newest_analyses


def _usecase_refs(*entities: Labeled) -> set[str]:
    return set(
        str(k).split("/")[-1]
        for k, v in itertools.chain(*(e.labels.items() for e in entities))
        if str(k).startswith("usecases.dyff.io/") and v is not None
    )


@app.get(
    f"/systems/{{id}}",
    tags=["systems"],
    summary="Get the data for the /systems/[id] view.",
    response_description="The data needed to populate the view.",
)
def systems_get(
    id: str,
    query_backend: _ViewQueryBackend = fastapi.Depends(get_query_backend),
) -> SystemSingleView:
    model = query_backend.get_model(id)
    services: list[InferenceService] = []
    if model is None:
        service = query_backend.get_inference_service(id)
        if service is None:
            raise fastapi.HTTPException(
                fastapi.status.HTTP_404_NOT_FOUND, detail=f"systems/{id}"
            )
        services = [service]
    else:
        services = list(
            query_backend.query_inference_services(
                InferenceServiceQueryRequest(model=model.id)
            )
        )

    measurements = query_backend.query_measurements(
        MeasurementQueryRequest(
            query=json.dumps({"inferenceService": [service.id for service in services]})
        )
    )

    safetycases = query_backend.query_safetycases(
        SafetyCaseQueryRequest(
            query=json.dumps({"inferenceService": [service.id for service in services]})
        )
    )

    # There could be many Measurements / SafetyCases with same .method
    method_ids = list(set(e.method.id for e in itertools.chain(measurements, safetycases)))  # type: ignore
    methods = query_backend.query_methods(
        MethodQueryRequest(query=json.dumps({"id": method_ids}))
    )

    comparisons = query_backend.query_safetycases(
        SafetyCaseQueryRequest(query=json.dumps({"method": method_ids}))
    )
    comparisons_by_method: dict[str, list[SafetyCase]] = {}
    for c in comparisons:
        by_method = comparisons_by_method.setdefault(c.method.id, [])
        by_method.append(c)

    newest_analyses = itertools.chain(
        # Always include all safetycases for the current system
        safetycases,
        # Only the newest run for each method for comparison systems
        *(
            _newest_analyses_grouped_by_system(
                comparisons_by_method[method.id]
            ).values()
            for method in methods
        ),
    )
    # SECURITY: Scores are published if the analysis that generated them is
    # published. These analyses all came from which came query_safetycases(),
    # which only returns published entities.
    scores = query_backend.unsafe_query_scores(
        [analysis.id for analysis in newest_analyses]
    )

    documentation_ids = list(
        itertools.chain(
            (method.id for method in methods),
            (service.id for service in services),
            [model.id] if model else [],
        )
    )

    # SECURITY: Documentation is published if its parent entity is published.
    # All of these IDs came from entities retrieved with 'query_xxx()', which
    # only returns published entities.
    documentation = query_backend.unsafe_query_documentation(documentation_ids)

    usecase_ids = _usecase_refs(*methods, *services, *([model] if model else []))
    usecases = query_backend.query_usecases(
        UseCaseQueryRequest(query=json.dumps({"id": list(usecase_ids)}))
    )

    return SystemSingleView(
        model=model,
        services=list(services),
        methods=list(methods),
        measurements=list(measurements),
        safetycases=list(safetycases),
        documentation=list(documentation),
        scores=list(scores),
        usecases=list(usecases),
    )


@app.get(
    f"/systems",
    tags=["systems"],
    summary="Get the data for the /systems view.",
    response_description="The data needed to populate the view.",
)
def systems_list(
    query_backend: _ViewQueryBackend = fastapi.Depends(get_query_backend),
) -> SystemListView:
    services = query_backend.query_inference_services(InferenceServiceQueryRequest())

    model_ids = set(
        service.model.id for service in services if service.model is not None
    )
    models = query_backend.query_models(
        ModelQueryRequest(query=json.dumps({"id": list(model_ids)}))
    )

    documentation_ids = list(e.id for e in itertools.chain(models, services))

    # SECURITY: Documentation is published if its parent entity is published.
    # All of these IDs came from entities retrieved with 'query_xxx()', which
    # only returns published entities.
    documentation = query_backend.unsafe_query_documentation(documentation_ids)

    return SystemListView(
        models=list(models),
        services=list(services),
        documentation=list(documentation),
    )


@app.get(
    f"/methods/{{id}}",
    tags=["methods"],
    summary="Get the data for the /methods/[id] view.",
    response_description="The data needed to populate the view.",
)
def methods_get(
    id: str,
    query_backend: _ViewQueryBackend = fastapi.Depends(get_query_backend),
) -> MethodSingleView:
    """Get the data for the /methods/[id] view."""
    method = query_backend.get_method(id)
    if method is None:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_404_NOT_FOUND, detail=f"methods/{id}"
        )

    measurements = query_backend.query_measurements(
        MeasurementQueryRequest(method=method.id)
    )

    safetycases = query_backend.query_safetycases(
        SafetyCaseQueryRequest(method=method.id)
    )

    service_ids = [
        e.scope.inferenceService  # type: ignore
        for e in itertools.chain(measurements, safetycases)
        if e.scope.inferenceService is not None  # type: ignore
    ]

    services = query_backend.query_inference_services(
        InferenceServiceQueryRequest(query=json.dumps({"id": service_ids}))
    )

    # If the Service is backed by the Model, we want to show the Model as the
    # representative of the "system"
    model_ids = set(
        service.model.id for service in services if service.model is not None
    )
    models = query_backend.query_models(
        ModelQueryRequest(query=json.dumps({"id": list(model_ids)}))
    )

    newest_analyses = _newest_analyses_grouped_by_system(safetycases)
    # SECURITY: Scores are published if the analysis that generated them is
    # published. These analyses are a subset of safetycases, which came from
    # query_safetycases(), which only returns published entities.
    scores = query_backend.unsafe_query_scores(
        [analysis.id for analysis in newest_analyses.values()]
    )

    documentation_ids = list(
        itertools.chain(
            [method.id],
            (service.id for service in services),
            (model.id for model in models),
        )
    )

    # SECURITY: Documentation is published if its parent entity is published.
    # All of these IDs came from entities retrieved with 'query_xxx()', which
    # only returns published entities.
    documentation = query_backend.unsafe_query_documentation(documentation_ids)

    usecase_ids = _usecase_refs(method)
    usecases = query_backend.query_usecases(
        UseCaseQueryRequest(query=json.dumps({"id": list(usecase_ids)}))
    )

    return MethodSingleView(
        method=method,
        measurements=list(measurements),
        safetycases=list(safetycases),
        services=list(services),
        models=list(models),
        documentation=list(documentation),
        scores=list(scores),
        usecases=list(usecases),
    )


@app.get(
    f"/methods",
    tags=["methods"],
    summary="Get the data for the /methods view.",
    response_description="The data needed to populate the view.",
)
def methods_list(
    query_backend: _ViewQueryBackend = fastapi.Depends(get_query_backend),
) -> MethodListView:
    methods = query_backend.query_methods(MethodQueryRequest())

    documentation_ids = list(e.id for e in methods)

    # SECURITY: Documentation is published if its parent entity is published.
    # All of these IDs came from entities retrieved with 'query_xxx()', which
    # only returns published entities.
    documentation = query_backend.unsafe_query_documentation(documentation_ids)

    usecase_ids = _usecase_refs(*methods)
    usecases = query_backend.query_usecases(
        UseCaseQueryRequest(query=json.dumps({"id": list(usecase_ids)}))
    )

    return MethodListView(
        methods=list(methods),
        documentation=list(documentation),
        usecases=list(usecases),
    )


_DyffEntityT = TypeVar("_DyffEntityT", bound=DyffEntity)


def _deduplicated(xs: Iterable[_DyffEntityT]) -> Iterable[_DyffEntityT]:
    ids: set[str] = set()
    for x in xs:
        old_len = len(ids)
        ids.add(x.id)
        if len(ids) > old_len:  # item was added
            yield x


@app.get(
    "/home",
    tags=["home"],
    summary="Get the data for the /home view.",
    response_description="The data needed to populate the view.",
)
def home(
    query_backend: _ViewQueryBackend = fastapi.Depends(get_query_backend),
) -> HomeView:
    limit = 3

    methods = query_backend.query_methods(
        MethodQueryRequest(
            status="Ready", orderBy="creationTime", order="descending", limit=limit
        )
    )
    usecases = list(
        query_backend.query_usecases(
            UseCaseQueryRequest(
                # FIXME: This needs to be a privileged feature, because doing
                # it with a label lets users affect the behavior of resources
                # in accounts they don't have access to.
                labels=json.dumps({"app.dyff.io/display-priority": "featured"}),
                status="Ready",
                orderBy="creationTime",
                order="descending",
                limit=limit,
            )
        )
    )
    if len(usecases) < limit:
        usecases.extend(
            query_backend.query_usecases(
                UseCaseQueryRequest(
                    status="Ready",
                    orderBy="creationTime",
                    order="descending",
                    limit=limit,
                )
            )
        )
        usecases = list(_deduplicated(usecases))
    models = query_backend.query_models(
        ModelQueryRequest(
            status="Ready", orderBy="creationTime", order="descending", limit=limit
        )
    )
    documentation_ids = list(
        itertools.chain(
            (method.id for method in methods),
            (model.id for model in models),
        )
    )
    # SECURITY: Documentation is published if its parent entity is published.
    # All of these IDs came from entities retrieved with 'query_xxx()', which
    # only returns published entities.
    documentation = query_backend.unsafe_query_documentation(documentation_ids)

    return HomeView(
        methods=list(methods),
        models=list(models),
        usecases=list(usecases),
        documentation=list(documentation),
    )
