# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# import os
#
# os.environ["BENTOML_DO_NOT_TRACK"] = "true"
#
# # try:
# #     from bentoml._internal.utils.analytics.usage_stats import do_not_track
# # except ImportError:
# #     pass
# # else:
# #     if not do_not_track():
# #         raise ImportError("Couldn't import BentoML with tracking disabled")
#
#
# from .config import config
#
