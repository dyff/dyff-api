# dyff-api

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/dyff/dyff-api?branch=main)](https://gitlab.com/dyff/dyff-api/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/dyff/dyff-api)](https://gitlab.com/dyff/dyff-api/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)

<!-- END BADGIE TIME -->

API server for the Dyff AI auditing platform.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## License

Copyright 2024 UL Research Institutes.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

## Quickstart

Disable the `dyff-api` deployment in your `dev-environment` local repo, [see
here for detailed
instructions](https://gitlab.com/dyff/dev-environment/-/blob/main/README.md#disabling-deployments)

Create a `settings.yaml` to customize the dev deployment of `dyff-api`:

```yaml
ingress:
  enabled: true
  className: nginx
  # Optional: additional host name proxy
  hosts:
    - host: api.dyff.io
      paths:
        - path: /
          pathType: ImplementationSpecific

extraEnvVarsConfigMap:
  DYFF_STORAGE__BACKEND: "dyff.storage.backend.s3.storage.S3StorageBackend"
  DYFF_STORAGE__S3__ENDPOINT: "s3.minio.dyff.local"
  DYFF_STORAGE__S3__INTERNAL_ENDPOINT: "http://minio.minio.svc.cluster.local:9000"
  DYFF_STORAGE__S3__ACCESS_KEY: "XXXXXXX" # (1)
  DYFF_RESOURCES__DATASETS__STORAGE__URL: "s3://kind-dyff-datasets-<unique id>" # (2)
  DYFF_RESOURCES__MODULES__STORAGE__URL: "s3://kind-dyff-modules-<unique id>" # (2)
  DYFF_API__AUTH__FRONTEND_CALLBACK_URL: "http://localhost:3000/auth/callback"

extraEnvVarsSecret:
  DYFF_STORAGE__S3__SECRET_KEY: "XXXXXXX" # (1) <!-- pragma: allowlist secret  -->
  DYFF_API__AUTH__MONGODB__CONNECTION_STRING: "XXXXXX" # (3)
  DYFF_API__QUERY__MONGODB__CONNECTION_STRING: "XXXXXX" # (3)
  DYFF_API__AUTH__API_KEY_SIGNING_SECRET: "XXXXXXX" # (4) <!-- pragma: allowlist secret  -->
  DYFF_API__AUTH__SESSION_SECRET: "XXXXXXX" # (4) <!-- pragma: allowlist secret  -->
  DYFF_API__AUTH__GOOGLE__CLIENT_ID: "XXXXXXX" #(5)
  DYFF_API__AUTH__GOOGLE__CLIENT_SECRET: "XXXXXXX" # (5) <!-- pragma: allowlist secret  -->
```

**(1)** Minio username and password from `credentials.yaml` generated in `dev-environment` setup

**(2)** S3 Bucket Unique ID's are generated dynamically. Log into minio.dyff.local to source them

**(3)** Source `X_CONNECTION_STRING` from `credentials.yaml`

- `DYFF_API__AUTH__MONGODB__CONNECTION_STRING`

  - Sourced from `mongodb.databases.auth.connection_string`

- `DYFF_API__QUERY__MONGODB__CONNECTION_STRING` and `DYFF_WORKFLOWS_SINK__MONGODB__CONNECTION_STRING`

  - Both sourced from `mongodb.databases.query.connection_string`

**(4)** Generate key signing and session secrets with `openssl rand -base64 40`

**(5)** Auth provider secrets are procured by the appropriate provider.

Run the `dyff-api` dev deployment:

```
skaffold dev
```

## Auth

For certain auth providers such as Google, owned domains may be required for
authorized redirect URI's. In our case, `api.dyff.local` is invalid, so we must
setup an additional reverse proxy:

In `/etc/hosts/`, add `api.dyff.io`

```
127.0.0.1       api.dyff.io
```

In `settings.yaml`, add to `ingress`

```yaml
ingress:
  enabled: true
  className: nginx
  hosts:
    - host: api.dyff.io
      paths:
        - path: /
          pathType: ImplementationSpecific
```
