# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import importlib

import pytest


@pytest.mark.parametrize(
    "module_name",
    [
        "dyff.api.tokens",
        "dyff.api.mgmt.accounts",
        "dyff.api.mgmt.tokens",
        "dyff.api.mgmt.roles",
        "dyff.api.mgmt",
        "dyff.api.mgmt.__main__",
        "dyff.api.timestamp",
        "dyff.api.typing",
        "dyff.api.api",
        # "dyff.api.server",
        "dyff.api.sanitize",
        # "dyff.api.auth",
        "dyff.api",
        "dyff.api.config",
        "dyff.api.dynamic_import",
        "dyff.api.public",
        "dyff.api.exceptions",
    ],
)
def test_import_module(module_name):
    importlib.import_module(module_name)
