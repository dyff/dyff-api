# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import argparse
import string
from pathlib import Path

code_template = """
object ${kind} {
  import _root_.io.dyff.schema.DyffPickler._
  import _root_.io.dyff.schema.SchemaImplicits._
  implicit val rw: ReadWriter[${kind}] = macroRW
  implicit def toJson(x: ${kind}): ujson.Obj = read[ujson.Obj](write(x))
}

"""


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--schema_dir", type=str)
    args = parser.parse_args()

    for f in Path(args.schema_dir).iterdir():
        kind = f.stem
        with open(f, "a") as fout:
            code = string.Template(code_template).substitute(kind=kind)
            fout.write(code)


if __name__ == "__main__":
    main()
