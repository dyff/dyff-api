#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from __future__ import annotations

import json
import sys
from typing import Any

import pydantic.schema

from dyff.schema import commands
from dyff.schema.platform import (
    Dataset,
    Evaluation,
    Family,
    Hazard,
    History,
    InferenceService,
    InferenceSession,
    Measurement,
    Method,
    Model,
    Module,
    Report,
    Revision,
    SafetyCase,
    UseCase,
)


def platform_schema() -> dict:
    labels_schema = r"""
    "labels": {
        "title": "Labels",
        "description": "A set of key-value labels for the resource. Used to specify identifying attributes of resources that are meaningful to users but do not imply semantics in the dyff system.\n\nThe keys are DNS labels with an optional DNS domain prefix. For example: 'my-key', 'your.com/key_0'. Keys prefixed with 'dyff.io/', 'subdomain.dyff.io/', etc. are reserved.\n\nThe label values are alphanumeric characters separated by '.', '-', or '_'.\n\nWe follow the kubernetes label conventions closely. See: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels",
        "type": "object",
        "patternProperties": {
        "^([a-zA-Z0-9]([-a-zA-Z0-9]{0,61}[a-zA-Z0-9])?(\\.[a-zA-Z0-9]([-a-zA-Z0-9]{0,61}[a-zA-Z0-9])?)*/)?[a-zA-Z0-9]([-a-zA-Z0-9]{0,61}[a-zA-Z0-9])?$": {
            "anyOf": [
            {
                "type": "string",
                "maxLength": 63,
                "pattern": "^([a-z0-9A-Z]([-_.a-z0-9A-Z]{0,61}[a-z0-9A-Z])?)?$"
            },
            {
                "type": "null"
            }
            ]
        }
        },
        "additionalProperties": {
        "anyOf": [
            {
            "type": "string",
            "maxLength": 63,
            "pattern": "^([a-z0-9A-Z]([-_.a-z0-9A-Z]{0,61}[a-z0-9A-Z])?)?$"
            },
            {
            "type": "null"
            }
        ]
        }
    },
    """

    top_level_types = [
        Dataset,
        Evaluation,
        Family,
        History,
        InferenceService,
        InferenceSession,
        Measurement,
        Method,
        Model,
        Module,
        Report,
        Revision,
        SafetyCase,
    ]

    top_level_type_names = [t.__name__ for t in top_level_types]

    label_value_template = """
    "anyOf": [

    ]
    """

    title = "Dyff Platform Data Model v0.1"

    schema: dict[str, Any] = {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "https://api.dyff.io/v0/schema/platform.json",
    }
    schema.update(
        pydantic.schema.schema(
            top_level_types,  # type: ignore
            title=title,
        )
    )

    for type_name, definition in schema["definitions"].items():
        # We supply defaults for "kind" and "schemaVersion" in the pydantic models,
        # which makes the generated schema list them as optional. We add them to
        # the "required" list and remove the default values, so that our validator
        # will reject objects that don't have these properties set.
        if type_name in top_level_type_names:
            properties = definition["properties"]
            properties["kind"].pop("default")
            properties["schemaVersion"].pop("default")
            required = set(definition["required"])
            required.add("kind")
            required.add("schemaVersion")
            definition["required"] = list(required)

            # Pydantic doesn't generate the .labels schema correctly. Specifically:
            # 1. It doesn't allow null as a value, even though the value is Optional[T]
            # 2. It generates additionalProperties, which allows adding keys that
            #    don't validate against patternProperties
            labels = properties["labels"]
            for k in labels["patternProperties"]:
                v = labels["patternProperties"][k]
                labels["patternProperties"][k] = {"anyOf": [v, {"type": "null"}]}
            labels["additionalProperties"] = False

    return schema


def commands_schema() -> dict:
    top_level_types = [
        commands.CreateEntity,
        commands.EditEntityDocumentation,
        commands.EditEntityLabels,
        commands.EditFamilyMembers,
        commands.ForgetEntity,
        commands.UpdateEntityStatus,
    ]

    top_level_type_names = [t.__name__ for t in top_level_types]

    title = "Dyff Platform Command Model v0.1"

    schema: dict[str, Any] = {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "https://api.dyff.io/v0/schema/commands.json",
    }
    schema.update(
        pydantic.schema.schema(
            top_level_types,  # type: ignore
            title=title,
        )
    )

    for type_name, definition in schema["definitions"].items():
        # We supply defaults for "command" and "schemaVersion" in the pydantic models,
        # which makes the generated schema list them as optional. We add them to
        # the "required" list and remove the default values, so that our validator
        # will reject objects that don't have these properties set.
        if type_name in top_level_type_names:
            properties = definition["properties"]
            properties["command"].pop("default")
            properties["schemaVersion"].pop("default")
            required = set(definition["required"])
            required.add("command")
            required.add("schemaVersion")
            definition["required"] = list(required)

    return schema


def monkey_patch__get_model_name_map(unique_models):
    """Process a set of models and generate unique names for them to be used as
    keys in the JSON Schema definitions. By default the names are the same as
    the class name. But if two models in different Python modules have the same
    name (e.g. "users.Model" and "items.Model"), the generated names will be
    based on the Python module path for those conflicting models to prevent
    name collisions.

    :param unique_models: a Python set of models
    :return: dict mapping models to names
    """
    name_model_map = {}
    conflicting_names: set[str] = set()
    for model in unique_models:
        model_name = pydantic.schema.normalize_name(model.__name__)
        if model_name in conflicting_names:
            model_name = pydantic.schema.get_long_model_name(model)
            name_model_map[model_name] = model
        elif model_name in name_model_map:
            conflicting_names.add(model_name)
            conflicting_model = name_model_map.pop(model_name)
            name_model_map[pydantic.schema.get_long_model_name(conflicting_model)] = (
                conflicting_model
            )
            name_model_map[pydantic.schema.get_long_model_name(model)] = model
        else:
            # !!! We changed this so that it always uses the long model name !!!
            model_name = pydantic.schema.get_long_model_name(model)
            name_model_map[model_name] = model
    return {v: k for k, v in name_model_map.items()}


# FIXME: We'd like to split the schema into multiple files that mirror the
# Python module structure, but Pydantic generates definitions for all
# referenced sub-models, so we would need to post-process the schema file and
# move the definitions to other files as appropriate. Pydantic does take care
# of name collisions by prefixing with the module path to get an FQN. Maybe we
# can force it to do that even if there are no collisions?
def joint_schema() -> dict:
    platform_types = [
        Dataset,
        Evaluation,
        Family,
        Hazard,
        History,
        InferenceService,
        InferenceSession,
        Measurement,
        Method,
        Model,
        Module,
        Report,
        Revision,
        SafetyCase,
        UseCase,
    ]

    commands_types = [
        commands.CreateEntity,
        commands.EditEntityDocumentation,
        commands.EditEntityLabels,
        commands.EditFamilyMembers,
        commands.ForgetEntity,
        commands.UpdateEntityStatus,
    ]

    platform_type_names = [
        f"dyff__schema__v0__r1__platform__{t.__name__}" for t in platform_types
    ]
    commands_type_names = [
        f"dyff__schema__v0__r1__commands__{t.__name__}" for t in commands_types
    ]

    title = "Dyff Platform Schema v0.1"

    schema: dict[str, Any] = {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "https://api.dyff.io/v0/schema/dyff.json",
    }
    pydantic.schema.get_model_name_map = monkey_patch__get_model_name_map
    schema.update(
        pydantic.schema.schema(
            platform_types + commands_types,  # type: ignore
            title=title,
        )
    )

    for type_name, definition in schema["definitions"].items():
        # We supply defaults for "kind" and "schemaVersion" in the pydantic models,
        # which makes the generated schema list them as optional. We add them to
        # the "required" list and remove the default values, so that our validator
        # will reject objects that don't have these properties set.
        if type_name in platform_type_names:
            properties = definition["properties"]
            properties["kind"].pop("default")
            properties["schemaVersion"].pop("default")
            required = set(definition["required"])
            required.add("kind")
            required.add("schemaVersion")
            definition["required"] = list(required)

            # Pydantic doesn't generate the .labels schema correctly. Specifically:
            # 1. It doesn't allow null as a value, even though the value is Optional[T]
            # 2. It generates additionalProperties, which allows adding keys that
            #    don't validate against patternProperties
            labels = properties["labels"]
            for k in labels["patternProperties"]:
                labels["patternProperties"][k]
                # labels["patternProperties"][k] = {"anyOf": [v, {"type": "null"}]}
            labels["additionalProperties"] = False
        elif type_name in commands_type_names:
            # We supply defaults for "command" and "schemaVersion" in the pydantic models,
            # which makes the generated schema list them as optional. We add them to
            # the "required" list and remove the default values, so that our validator
            # will reject objects that don't have these properties set.
            properties = definition["properties"]
            properties["command"].pop("default")
            properties["schemaVersion"].pop("default")
            required = set(definition["required"])
            required.add("command")
            required.add("schemaVersion")
            definition["required"] = list(required)

    return schema


with open(sys.argv[1] + "/dyff.json", "w") as fout:
    json.dump(joint_schema(), fout, indent=2)
# with open(sys.argv[1] + "/platform.json", "w") as fout:
#     json.dump(platform_schema(), fout, indent=2)
# with open(sys.argv[1] + "/commands.json", "w") as fout:
#     json.dump(commands_schema(), fout, indent=2)
