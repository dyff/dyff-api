#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import json
import sys

from dyff.api.api import app

# See: https://github.com/tiangolo/fastapi/issues/1173#issuecomment-605664503
with open(sys.argv[1], "w") as fout:
    json.dump(
        app.openapi(),
        fout,
        indent=2,
    )
