# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import argparse
import shlex
import string
import sys
from functools import lru_cache
from typing import Optional

import pydantic

from dyff.schema.platform import DyffSchemaBaseModel, ModelStorageMedium


class NodeProfile(DyffSchemaBaseModel):
    name: str = pydantic.Field(description="The name of the profile.")
    cpuCount: int = pydantic.Field(description="Number of vCPUs available on the node.")
    memoryGiB: int = pydantic.Field(description="Memory available on the node.")
    ephemeralStorageGiB: int = pydantic.Field(
        description="Ephemeral storage available on the node."
    )
    gpuType: Optional[str] = pydantic.Field(
        default=None, description="Type of GPU available on the node."
    )
    gpuCount: int = pydantic.Field(
        default=0, description="Number of GPUs available on the node."
    )
    annnotations: dict[str, str] = pydantic.Field(
        default_factory=dict,
        description="Annotations to add to Pods when using this profile.",
    )


# Notes:
#
# * All Autopilot GPU Pods use the Accelerator compute class by default
#
# * "Unless specified, the maximum ephemeral storage supported is 122 GiB"
#   https://cloud.google.com/kubernetes-engine/docs/concepts/autopilot-resource-requests#hardware-min-max


@lru_cache
def available_node_profiles() -> list[NodeProfile]:
    return [
        # Nvidia L4 GPUs
        # NodeProfile(
        #     name="nvidia-l4-1",
        #     cpuCount=29,
        #     memoryGiB=101,
        #     ephemeralStorageGiB=122,
        #     gpuType="nvidia.com/gpu-l4",
        #     gpuCount=1,
        # ),
        # NodeProfile(
        #     name="nvidia-l4-2",
        #     cpuCount=21,
        #     memoryGiB=69,
        #     ephemeralStorageGiB=122,
        #     gpuType="nvidia.com/gpu-l4",
        #     gpuCount=2,
        # ),
        # NodeProfile(
        #     name="nvidia-l4-4",
        #     cpuCount=45,
        #     memoryGiB=163,
        #     ephemeralStorageGiB=122,
        #     gpuType="nvidia.com/gpu-l4",
        #     gpuCount=4,
        # ),
        # NodeProfile(
        #     name="nvidia-l4-8",
        #     cpuCount=93,
        #     memoryGiB=349,
        #     ephemeralStorageGiB=122,
        #     gpuType="nvidia.com/gpu-l4",
        #     gpuCount=8,
        # ),
        # Nvidia A100 GPUs
        NodeProfile(
            name="nvidia-a100-1",
            cpuCount=9,
            memoryGiB=60 - (1 * 16),
            ephemeralStorageGiB=122,
            gpuType="nvidia.com/gpu-a100",
            gpuCount=1,
        ),
        NodeProfile(
            name="nvidia-a100-2",
            cpuCount=20,
            memoryGiB=134 - (2 * 16),
            ephemeralStorageGiB=122,
            gpuType="nvidia.com/gpu-a100",
            gpuCount=2,
        ),
        NodeProfile(
            name="nvidia-a100-4",
            cpuCount=44,
            memoryGiB=296 - (4 * 16),
            ephemeralStorageGiB=122,
            gpuType="nvidia.com/gpu-a100",
            gpuCount=4,
        ),
        NodeProfile(
            name="nvidia-a100-8",
            cpuCount=92,
            memoryGiB=618 - (8 * 16),
            ephemeralStorageGiB=122,
            gpuType="nvidia.com/gpu-a100",
            gpuCount=8,
        ),
        NodeProfile(
            name="nvidia-a100-16",
            cpuCount=92,
            memoryGiB=1250 - (16 * 16),
            ephemeralStorageGiB=122,
            gpuType="nvidia.com/gpu-a100",
            gpuCount=16,
        ),
        # Nvidia H100 GPUs
        NodeProfile(
            name="nvidia-h100-8",
            cpuCount=206,
            memoryGiB=1795 - (8 * 16),
            ephemeralStorageGiB=5250,
            gpuType="nvidia.com/gpu-h100",
            gpuCount=8,
        ),
    ]


def matching_node_profiles(
    *,
    cpuCount: Optional[int] = None,
    memoryGiB: Optional[int] = None,
    ephemeralStorageGiB: Optional[int] = None,
    gpuType: Optional[str] = None,
    gpuCount: Optional[int] = None,
) -> list[NodeProfile]:
    matches: list[NodeProfile] = []
    for node in available_node_profiles():
        if cpuCount is not None and cpuCount > node.cpuCount:
            continue
        if memoryGiB is not None and memoryGiB > node.memoryGiB:
            continue
        if (
            ephemeralStorageGiB is not None
            and ephemeralStorageGiB > node.ephemeralStorageGiB
        ):
            continue
        if gpuType is not None and gpuType != node.gpuType:
            continue
        if gpuCount is not None and gpuCount > node.gpuCount:
            continue
        matches.append(node)
    return matches


def main() -> None:
    template_args: dict[str, str] = {
        # Model
        ## Required
        "hf_name": "The name of the model on HuggingFace (e.g., 'allenai/olmo-7b')",
        "hf_revision": "The git revision of the model on HuggingFace",
        "model_size": "The size of the downloaded model files in k8s resource notation",
        ## Optional
        "model_weights_pattern": "An allowPattern for the weight files. Default: '*.safetensors'",
        "model_storage_medium": "ModelStorageMedium for the model. Default: FUSEVolume",
        # Service
        ## Required
        "model_id": "The ID of the Model in the Dyff system",
        "inference_api": "One of: 'completions', 'embeddings'",
        "embedding_size": "Size of embedding vector. Required if inference_api == 'embeddings'.",
        ## Optional
        "service_name": "Name of InferenceService. Default: '<model_name>/<inference_api>'.",
        "node_count": "The number of nodes in one service instance. Default: 1",
        "gpu_count": "The number of GPUs per node/replica. Default: 1",
        "gpu_type": "The type of GPU. Default: 'nvidia.com/gpu-a100'",
        "memory": "The amount of memory (RAM) per node/replica, in k8s resource notation. Default: computed from configuration",
        "dtype": "The weight dtype to use at runtime. Default: model default",
        "trust_remote_code": "Use trust_remote_code=True in HF APIs. Default: False",
        "service_args": "Command line arguments to forward to the service. Should be a single string; will be processed with shlex.split().",
        "inference_args": "Arguments to pass to inference calls. Must be a string encoding of a JSON object.",
        # Session
        ## Required
        "service_id": "The ID of the InferenceService in the Dyff system",
    }

    # These are GKE-specific
    # https://cloud.google.com/kubernetes-engine/docs/concepts/autopilot-resource-requests#hardware-min-max
    # gpu_count_to_memory: dict[int, str] = {
    #     1: "60Gi",
    #     2: "134Gi",
    #     4: "296Gi",
    #     8: "618Gi",
    #     16: "1250Gi",
    # }

    gpu_types: set[str] = {
        "nvidia.com/gpu-t4",
        "nvidia.com/gpu-l4",
        "nvidia.com/gpu-a100",
        "nvidia.com/gpu-a100-80gb",
        "nvidia.com/gpu-h100",
        "nvidia.com/gpu-h200",
    }

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "resource_kind", type=str, choices=["model", "service", "session"]
    )
    for template_arg, help in template_args.items():
        parser.add_argument(f"--{template_arg}", type=str, default=None, help=help)
    args = parser.parse_args()

    if args.resource_kind in ["service", "session"]:
        if args.service_name:
            resource_name = args.service_name
        else:
            resource_name = f"{args.hf_name}/{args.inference_api}"
    else:
        resource_name = args.hf_name
    resource_plural = f"{args.resource_kind}s"

    template_file = f"{resource_plural}/{args.resource_kind}.py.template"
    output_file = f"{resource_plural}/{resource_name.replace('/', '--')}.py"

    args_dict = vars(args)
    substitutions = {
        k: v for k, v in args_dict.items() if k in template_args and v is not None
    }
    if substitutions.get("service_name") is None:
        substitutions["service_name"] = f"{args.hf_name}/{args.inference_api}"
    if substitutions.get("service_args") is None:
        substitutions["service_args"] = ""
    if substitutions.get("inference_args") is None:
        substitutions["inference_args"] = ""
    else:
        substitutions["inference_args"] = substitutions["inference_args"].replace(
            '"', '\\"'
        )
    if substitutions.get("node_count") is None:
        substitutions["node_count"] = 1
    if substitutions.get("gpu_count") is None:
        substitutions["gpu_count"] = 1
    if substitutions.get("gpu_type") is None:
        substitutions["gpu_type"] = "nvidia.com/gpu-a100"
    if substitutions.get("dtype") is None:
        substitutions["dtype"] = ""
    if substitutions.get("embedding_size") is None:
        substitutions["embedding_size"] = ""
    if substitutions.get("trust_remote_code") is None:
        substitutions["trust_remote_code"] = ""
    if substitutions.get("model_weights_pattern") is None:
        substitutions["model_weights_pattern"] = "*.safetensors"
    if substitutions.get("model_storage_medium") is None:
        substitutions["model_storage_medium"] = ModelStorageMedium.FUSEVolume.value

    if (gpu_type := substitutions["gpu_type"]) not in gpu_types:
        raise ValueError(f"gpu_type={gpu_type}")

    if substitutions.get("memory") is None:
        substitutions["memory"] = ""

    substitutions["generate_command"] = " ".join(
        shlex.quote(s) for s in ["python"] + list(sys.argv)
    )

    with open(template_file, "r") as fin:
        template = string.Template(fin.read())

    with open(output_file, "w") as fout:
        fout.write(template.substitute(substitutions))

    print(output_file)

    if args.resource_kind == "service":
        # Generate the manifest to create the ROX volume. This is associated
        # with the 'service' step because we don't have the model ID yet in
        # the 'model' step.
        template_file = f"models/model-rox-volume.yaml.template"
        output_file = f"models/{args.hf_name.replace('/', '--')}--rox.yaml"

        with open(template_file, "r") as fin:
            template = string.Template(fin.read())

        with open(output_file, "w") as fout:
            fout.write(template.substitute(substitutions))

        print(output_file)


if __name__ == "__main__":
    main()
