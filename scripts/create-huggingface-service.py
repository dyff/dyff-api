# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import argparse
import json
import subprocess


def main():
    template_args = ["hf_name", "hf_revision", "model_size", "gpu_count"]
    generate_resource_script = "generate-huggingface-resource-scripts.py"

    parser = argparse.ArgumentParser()
    parser.add_argument("--execute", type=bool, default=False)
    for template_arg in template_args:
        parser.add_argument(f"--{template_arg}", type=str, default=None)
    args = parser.parse_args()
    args_dict = vars(args)
    template_args = {
        k: v for k, v in args_dict.items() if k in template_args and v is not None
    }

    subprocess_args = [f"--{k}={v}" for k, v in template_args.items()]

    model_script = subprocess.run(
        ["python", generate_resource_script, "model"] + subprocess_args,
        capture_output=True,
        text=True,
    ).stdout.strip()
    print(model_script)

    model_id = ""
    if args.execute:
        model_json = subprocess.run(
            ["python", model_script] + subprocess_args,
            capture_output=True,
            text=True,
        )
        model_dict = json.loads(model_json)
        model_id = model_dict["id"]
        print(f"model: {model_id}")

    subprocess_args.append(f"--model_id={model_id}")
    service_script = subprocess.run(
        ["python", generate_resource_script, "service"] + subprocess_args,
        capture_output=True,
        text=True,
    ).stdout.strip()
    print(service_script)

    service_id = ""
    if args.execute:
        service_json = subprocess.run(
            ["python", service_script] + subprocess_args,
            capture_output=True,
            text=True,
        )
        service_dict = json.loads(service_json)
        service_id = service_dict["id"]
        print(f"service: {service_id}")

    subprocess_args.append(f"--service_id={service_id}")
    session_script = subprocess.run(
        ["python", generate_resource_script, "session"] + subprocess_args,
        capture_output=True,
        text=True,
    ).stdout.strip()
    print(session_script)

    print("Don't forget to re-create the ROX volume before running the session!")


if __name__ == "__main__":
    main()
