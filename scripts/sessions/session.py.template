# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# This script is auto-generated with the following command:
# ${generate_command}

from __future__ import annotations

import os
import time
from datetime import datetime, timedelta

from dyff.client import Client
from dyff.schema.requests import InferenceSessionCreateRequest

ENDPOINT: str | None = os.environ.get("DYFF_API_ENDPOINT")
API_TOKEN: str = os.environ["DYFF_API_TOKEN"]
ACCOUNT: str = "public"

dyffapi = Client(api_token=API_TOKEN, endpoint=ENDPOINT)

service_id = "${service_id}"
service_name = "${service_name}"
service = dyffapi.inferenceservices.get(service_id)
assert service.name == service_name
assert service.account == ACCOUNT
assert service.status == "Ready"

session_request = InferenceSessionCreateRequest(
    account=ACCOUNT,
    inferenceService=service_id,
    expires=datetime.now() + timedelta(days=1),
    replicas=1,
    useSpotPods=True,
)

session_and_token = dyffapi.inferencesessions.create(session_request)
session = session_and_token.inferencesession
session_id = session.id
session_token = session_and_token.token
print(session.json(indent=2))

# Create an inference client using the default interface specified for the
# InferenceService that's being run in the session
interface = session.inferenceService.interface
inference_client = dyffapi.inferencesessions.client(
    session_id,
    session_token,
    interface=interface,
)

# Starting the session can take some time, especially if you requested a GPU
# We'll get an exception if the session isn't up yet.
while True:
    try:
        y = inference_client.infer({"text": "Open the pod bay doors, Hal!"})
        print(y)
    except Exception:
        print(f"[{datetime.now()}]: not ready")
        time.sleep(30)
    else:
        break
