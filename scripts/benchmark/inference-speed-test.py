# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from __future__ import annotations

import argparse
import asyncio
import functools
import os
import random
from datetime import datetime
from pathlib import Path
from typing import Any

import aiohttp

from dyff.client import Client


async def infer(
    session: aiohttp.ClientSession,
    token: str,
    url: str,
    prompt: dict[str, Any],
) -> dict[str, Any]:
    """Post an inference request.

    Parameters:
      session: Shared ClientSession instance
      url: URL of the inference service
      item_id: The unique identifier of the instance
      http_payload: Tuple containing 'headers' and 'data' fields for the request

    Returns:
      (item_id, response_json): ``item_id`` is the same as the input item_id,
        ``response_json`` contains the response body as a JSON object.
    """
    for _ in range(5):
        try:
            timeout = aiohttp.ClientTimeout(total=30)
            headers = {
                "content-type": "application/json",
                "Authorization": f"Bearer {token}",
            }
            async with session.post(
                url,
                headers=headers,
                json=prompt,
                timeout=timeout,
                raise_for_status=True,
            ) as response:
                return await response.json()
        except aiohttp.ClientResponseError as ex:
            # The inference call fails with 502_BAD_GATEWAY occasionally for
            # some reason
            if ex.status == 502:
                await asyncio.sleep(1)
            else:
                raise
    else:
        raise AssertionError()


async def main() -> None:
    parser = argparse.ArgumentParser(
        description="Measure throughput of an inference session"
    )
    parser.add_argument("--session", help="The session ID")
    parser.add_argument(
        "--num_prompts", type=int, default=1000, help="Number of prompts to run"
    )
    parser.add_argument(
        "--prompt_length", type=int, default=20, help="Number of words in each prompt"
    )
    parser.add_argument(
        "--client",
        choices=["raw", "dyff-client"],
        default="raw",
        help="How to make requests",
    )
    parser.add_argument(
        "--inference_endpoint",
        default="generate",
        help="Inference endpoint on the service",
    )
    args = parser.parse_args()

    api_token: str = os.environ["DYFF_API_KEY"]
    api_endpoint: str = os.environ.get("DYFF_API_ENDPOINT", "https://api.dyff.io/v0")

    dyffapi = Client(api_key=api_token, endpoint=api_endpoint)
    session_token = dyffapi.inferencesessions.token(args.session)

    with open(Path(__file__).resolve().parent / "words_alpha.txt", "r") as fin:
        words = [line.strip() for line in fin]

    prompts = [
        {"prompt": " ".join(random.choices(words, k=args.prompt_length))}
        for _ in range(args.num_prompts)
    ]

    if args.client == "dyff-client":
        session_client = dyffapi.inferencesessions.client(
            args.session,
            session_token,
            endpoint=args.inference_endpoint,
        )

        loop = asyncio.get_event_loop()
        then = datetime.now()
        await asyncio.gather(
            *(
                loop.run_in_executor(
                    None, functools.partial(session_client.infer, prompt)
                )
                for prompt in prompts
            )
        )
        now = datetime.now()
    elif args.client == "raw":
        async with aiohttp.ClientSession(
            connector=aiohttp.TCPConnector(force_close=True)
        ) as session:
            url = "/".join(
                [
                    api_endpoint,
                    "inferencesessions",
                    args.session,
                    "infer",
                    args.inference_endpoint,
                ]
            )
            then = datetime.now()
            await asyncio.gather(
                *(infer(session, api_token, url, prompt) for prompt in prompts)
            )
            now = datetime.now()

    duration = now - then
    print(f"ran {args.num_prompts} prompts in {duration}")


if __name__ == "__main__":
    asyncio.run(main())
