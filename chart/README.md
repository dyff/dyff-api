# dyff-api

[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/dyff-api)](https://artifacthub.io/packages/search?repo=dyff-api)

API server for the Dyff AI auditing platform.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Installation

```bash
helm install dyff-api oci://registry.gitlab.com/dyff/charts/dyff-api
```

## Removal

Delete the chart:

```bash
helm uninstall dyff-api
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| args | list | `["--proxy-headers","--host","0.0.0.0","--port","8080"]` | Arguments to pass to the dyff-api container. |
| auth.backendClass | string | `"dyff.storage.backend.mongodb.auth.MongoDBAuthBackend"` | Database backend to use for account management. |
| auth.google.clientId | string | `""` | Client ID to enable the Google OIDC integration. |
| auth.google.clientSecret | string | `""` | Client secret to enable the Google OIDC integration. |
| auth.mongodb.connectionString | string | `"mongodb+srv://mongodb.mongodb.svc.cluster.local/accounts?ssl=false"` |  |
| auth.mongodb.database | string | `"accounts"` |  |
| auth.redirectUrl | string | `""` | Redirect the user to this URL after successful authentication. |
| auth.sessionSecret | string | `""` | Provide a session secret. If not specified, a random one is generated on each install. Changing this value invalidates all sessions. |
| auth.signingSecret | string | `""` | Provide an API key signing secret. If not specified, a random one is generated on each install. Changing this value invalidates all API keys. |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.maxReplicas | int | `100` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| command | list | `["uvicorn","dyff.api.server:app"]` | Command to run for the dyff-api container. |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` |  |
| containerSecurityContext.capabilities.drop[0] | string | `"ALL"` |  |
| containerSecurityContext.privileged | bool | `false` |  |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` |  |
| containerSecurityContext.runAsGroup | int | `1001` |  |
| containerSecurityContext.runAsNonRoot | bool | `true` |  |
| containerSecurityContext.runAsUser | int | `1001` |  |
| extraEnvVarsConfigMap | object | `{}` | Set environment variables in the `dyff-api` Deployment via ConfigMap. |
| extraEnvVarsSecret | object | `{}` | Set environment variables in the `dyff-api` Deployment via Secret. |
| fullnameOverride | string | `""` |  |
| hostAliases | list | `[]` | Entries to add to the pod's `/etc/hosts` file. |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"registry.gitlab.com/dyff/dyff-api"` |  |
| image.tag | string | `""` | Overrides the image tag whose default is the chart appVersion. |
| imagePullSecrets | list | `[]` |  |
| ingress.annotations | object | `{}` |  |
| ingress.className | string | `""` |  |
| ingress.enabled | bool | `false` |  |
| ingress.hosts[0].host | string | `"api.dyff.local"` |  |
| ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| ingress.tls | list | `[]` |  |
| kafka.bootstrapServer | string | `"kafka.kafka.svc.cluster.local"` |  |
| kafka.topics.events | string | `"dyff.workflows.events"` |  |
| kafka.topics.state | string | `"dyff.workflows.state"` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| podAnnotations | object | `{}` |  |
| podLabels | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| query.mongodb.connectionString | string | `"mongodb+srv://mongodb.mongodb.svc.cluster.local/workflows?ssl=false"` |  |
| query.mongodb.database | string | `"workflows"` |  |
| replicaCount | int | `1` |  |
| resources | object | `{}` |  |
| service.port | int | `8080` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.automount | bool | `true` |  |
| serviceAccount.create | bool | `true` |  |
| serviceAccount.name | string | `""` |  |
| storage.backend | string | `"s3"` |  |
| storage.s3.accessKey | string | `""` | S3 access key. |
| storage.s3.backendClass | string | `"dyff.storage.backend.s3.storage.S3StorageBackend"` | S3 storage backend class to use. |
| storage.s3.external.endpoint | string | `"s3.minio.dyff.local"` | External S3 endpoint reachable from outside the network. |
| storage.s3.internal.enabled | bool | `true` | Enable internal network connection. |
| storage.s3.internal.endpoint | string | `"http://minio.minio.svc.cluster.local:9000"` | Internal S3 endpoint reachable on the same private network, if applicable. Needed for some network configurations. |
| storage.s3.secretKey | string | `""` | S3 secret key. |
| storage.urls.datasets | string | `"s3://dyff/datasets"` | Bucket URL where datasets will be stored. |
| storage.urls.measurements | string | `"s3://dyff/measurements"` | Bucket URL where measurements will be stored. |
| storage.urls.modules | string | `"s3://dyff/modules"` | Bucket URL where modules will be stored. |
| storage.urls.outputs | string | `"s3://dyff/outputs"` | Bucket URL where outputs will be stored. |
| storage.urls.reports | string | `"s3://dyff/reports"` | Bucket URL where reports will be stored. |
| storage.urls.safetycases | string | `"s3://dyff/safetycases"` | Bucket URL where safetycases will be stored. |
| tolerations | list | `[]` |  |
| topologySpreadConstraints | list | `[]` |  |
| volumeMounts | list | `[]` | Additional volumeMounts on the output Deployment definition. |
| volumes | list | `[]` | Additional volumes on the output Deployment definition. |
| workflows.namespace | string | `"workflows"` | Kubernetes namespace where workflows will run. |

## License

Copyright 2024 UL Research Institutes.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
