{{- define "dyff-api.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}


{{- define "dyff-api.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


{{- define "dyff-api.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}


{{- define "dyff-api.labels" -}}
helm.sh/chart: {{ include "dyff-api.chart" . }}
{{ include "dyff-api.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "dyff-api.selectorLabels" -}}
app.kubernetes.io/name: {{ include "dyff-api.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "dyff-api.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "dyff-api.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "dyff-api.signing-secret" -}}
{{- if .Values.auth.signingSecret }}
{{- .Values.auth.signingSecret }}
{{- else }}
{{- randAlphaNum 64 }}
{{- end }}
{{- end }}

{{- define "dyff-api.session-secret" -}}
{{- if .Values.auth.sessionSecret }}
{{- .Values.auth.sessionSecret }}
{{- else }}
{{- randAlphaNum 64 }}
{{- end }}
{{- end }}
